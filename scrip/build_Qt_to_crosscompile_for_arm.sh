#!/bin/bash
clear


echo -e " ____Build Qt to crosscompile for arm_____ "
echo -e " ---------------------------- "
echo
echo -e " \033[0;97;2:31;5m author  : chuongtd4@gmail.com\033[0m";
echo
echo -e " \033[39;31;2:39;5m date    : 06/09/2019\033[0m";

echo -e " ---------------------------- "
echo
echo -e " \033[1;36mDBG  \033[0m \033[0;90m=\033[0m \033[0;34m1\033[0m";
echo -e " \033[1;36mDBG_1\033[0m \033[0;90m=\033[0m \033[0;34m0\033[0m"
tput ed
tput sgr0
echo

cd ~/
echo  $(mkdir build.everywhere/)
cd ~/build.everywhere
export dir=~/build.everywhere
echo $dir

rsync -avz -r root@192.168.1.58:/lib 		$dir/sysroot
rsync -avz -r root@192.168.1.58:/usr/include 	$dir/sysroot/usr
rsync -avz -r root@192.168.1.58:/usr/lib      $dir/sysroot/usr
#rsync -avz root@192.168.1.58:/opt/vc		$dir/sysroot/opt

git clone https://github.com/trandinhchuong/sysroot-relativelinks.git
python ./sysroot-relativelinks/sysroot-relativelinks.py sysroot

git clone git://code.qt.io/qt/qtbase.git -b "5.9"
cd qtbase

./configure -opensource \
 -confirm-license \
 -platform linux-g++-64 \
 -xplatform linux-aarch64-gnu-g++ \
 -no-opengl -device-option CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- \
 -sysroot  $dir/sysroot \
 -prefix   $dir \
 -extprefix  $dir \
 -hostprefix $dir \
 -v -nomake examples \
 -nomake tests \
 -no-use-gold-linker \
 -opensource \
 -confirm-license

make -j4
make install
file $dir/bin/qmake

echo -e "config qt .pro file project \n
CONFIG += arm64 #X64
X64:{
INCLUDEPATH += /usr/local/include/opencv4
LIBS +=/usr/local/lib/*.so
}
else{
INCLUDEPATH += /opt/opencv.arm/include/opencv4/
LIBS += ~/build.everywhere/lib/*.so
}
"
