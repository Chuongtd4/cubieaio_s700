#!/bin/bash
clear


echo -e " ____Build Qt to crosscompile for arm_____ "
echo -e " ---------------------------- "
echo
echo -e " \033[0;97;2:31;5m author  : chuongtd4@gmail.com\033[0m";
echo
echo -e " \033[39;31;2:39;5m date    : 06/09/2019\033[0m";

echo -e " ---------------------------- "
echo
echo -e " \033[1;36mDBG  \033[0m \033[0;90m=\033[0m \033[0;34m1\033[0m";
echo -e " \033[1;36mDBG_1\033[0m \033[0;90m=\033[0m \033[0;34m0\033[0m"
tput ed
tput sgr0
echo

make -C /home/chuong/megasyn/s700-sdk/kernel/ ARCH=arm64 CROSS_COMPILE=../toolchain/aarch64-linux-gnu/bin/aarch64-linux-gnu-  M=$PWD modules
