#!/bin/bash

#!/bin/bash
clear

echo -e " ____install g++ 5_____ "
echo -e " ---------------------------- "
echo
echo -e " \033[0;97;2:31;5m author  : chuongtd4@gmail.com\033[0m";
echo
echo -e " \033[39;31;2:39;5m date    : 06/09/2019\033[0m";

echo -e " ---------------------------- "
echo
echo -e " \033[1;36mDBG  \033[0m \033[0;90m=\033[0m \033[0;34m1\033[0m";
echo -e " \033[1;36mDBG_1\033[0m \033[0;90m=\033[0m \033[0;34m0\033[0m"
tput ed
tput sgr0
echo


sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt-get update
sudo apt-get install gcc-5.1 g++-5.1

echo -e "update"
sudo apt-get install g++-5
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 60 --slave /usr/bin/g++ g++ /usr/bin/g++-5

