#!/bin/bash
clear

echo -e " ____compiler opencv for action s700_____ "
echo -e " ---------------------------- "
echo
echo -e " \033[0;97;2:31;5m author  : chuongtd4@gmail.com\033[0m";
echo
echo -e " \033[39;31;2:39;5m date    : 06/09/2019\033[0m";

echo -e " ---------------------------- "
echo
echo -e " \033[1;36mDBG  \033[0m \033[0;90m=\033[0m \033[0;34m1\033[0m";
echo -e " \033[1;36mDBG_1\033[0m \033[0;90m=\033[0m \033[0;34m0\033[0m"
tput ed
tput sgr0
echo



mkdir ~/opencv.arm
cd ~/opencv.arm

if [ -d "opencv" ] 
then
    echo -e "da ton tai thu muc opencv"
else
    echo "dang tai ve sudo git clone https://github.com/opencv/opencv.git" 
    git clone https://github.com/opencv/opencv.git \
    cd  opencv
    git checkout 3.4
fi

if [ -d "opencv_contrib" ]
then
    echo -e "da ton tai thu muc opencv_contrib"
    cd ~/opencv.arm/
else
cd  ~/opencv.arm/
git clone https://github.com/opencv/opencv_contrib.git
cd ~/opencv.arm/opencv_contrib
git checkout 3.4
fi

cd ~/opencv.arm/opencv/
echo -e "tao thu muc build ~/opencv.arm/opencv/build.arm"
mkdir ~/opencv.arm/opencv/build.arm
echo -e "di chuyen toi ~//opencv.arm/opencv/build.arm"
cd ~/opencv.arm/opencv/build.arm

export dir=/usr/bin
export option="-D CMAKE_BUILD_TYPE=RELEASE  -D WITH_OPENCL=OFF -D BUILD_TESTS=OFF -D BUILD_PERF_TESTS=OFF -D WITH_FFMPEG=ON"

cmake -DCMAKE_INSTALL_PREFIX:PATH=~/opencv.arm/ \
-D CMAKE_C_COMPILER=$dir/aarch64-linux-gnu-gcc  \
-D CMAKE_CXX_COMPILER=$dir/aaarch64-linux-gnu-g++ \
-D CMAKE_LINKER=$dir/aarch64-linux-gnu-ld \
-D CMAKE_AR=$dir/aarch64-linux-gnu-gcc-ar \
-D SOFTFP=ON \
-D CMAKE_TOOLCHAIN_FILE=~/opencv.arm/opencv/platforms/linux/aarch64-gnu.toolchain.cmake ~/opencv.arm/opencv \
-D OPENCV_EXTRA_MODULES_PATH=~/opencv.arm/opencv_contrib/modules ~/opencv.arm/opencv/ \
-D CMAKE_BUILD_TYPE=Release \
-D BUILD_opencv_nonfree=OFF \
-D BUILD_TIFF=ON \
-D WITH_CmakeUDA=OFF \
-D ENABLE_AVX=OFF \
-D WITH_OPENGL=OFF \
-D WITH_OPENCL=OFF \
-D WITH_IPP=OFF \
-D WITH_TBB=ON \
-D BUILD_TBB=ON \
-D WITH_EIGEN=OFF \
-D WITH_V4L=ON \
-D WITH_VTK=ON \
-D WITH_GTK_2_X=ON \
-D BUILD_TESTS=OFF \
-D BUILD_PERF_TESTS=OFF \
-D CMAKE_BUILD_TYPE=RELEASE \


echo -e "cai dat cac goi thu vien phu thuoc"
#sudo apt-get install ffmpeg
#sudo apt-get install libv4l-dev
#
sed -i -e 's/#define __STDC_CONSTANT_MACROS/define __STDC_CONSTANT_MACROS/g' ../cmake/checks/ffmpeg_test.cpp


echo -e "configure xong /opt/opencv.arm/opencv/build.arm"
cd ~/opencv.arm/opencv/build.arm
echo -e "bat dau make "
sudo make -j4
sudo make install

echo -e "Sau khi cài đặt thành công, thư mục cài đặt ~/opencv.arm chứa 4 thư mục moi bin, include, lib, share. Thư mục lib chứa các files libcv.a, libcvaux.a,

libcxcore.a, libhighgui.a, libml.a, libcv.so.1.0.0, libcvaux.so.1.0.0, libcxcore.so.1.0.0, libhighgui.so.1.0.0, libml.so.1.0.0 "
echo -e "bat dau coppy thu vien da build len kit"
echo -e " kiem tra thu muc tren kit, neu chua co thi mkdir /usr/lib mkdir /usr/include"
make 
scp -r ~/opencv.arm/include/* root@192.168.1.58:/usr/include/
scp -r ~/opencv.arm/lib/*     root@192.168.1.58:/usr/lib/

echo -e "INCLUDEPATH += ~/opencv.arm/include/opencv4/
         LIBS +=        ~/opencv.arm/lib/*.so"
echo -e "INCLUDEPATH += /usr/local/include/opencv4
	 LIBS +=/usr/local/lib/*.so"

echo -e "config qt .pro file project \n
CONFIG += arm64 #X64
X64:{
INCLUDEPATH += /usr/local/include/opencv4
LIBS +=/usr/local/lib/*.so
}
else{
INCLUDEPATH += /opt/opencv.arm/include/opencv4/
LIBS += ~/opencv.arm/lib/*.so
}
"
echo -e "sudo apt-get autoremove opencv-doc opencv-data libopencv-dev libopencv2.4-java libopencv2.4-jni
python-opencv libopencv-core2.4 libopencv-gpu2.4 libopencv-ts2.4 libopencv-photo2.4 libopencv-contrib2.4
libopencv-imgproc2.4 libopencv-superres2.4 libopencv-stitching2.4 libopencv-ocl2.4 libopencv-legacy2.4
libopencv-ml2.4 libopencv-video2.4 libopencv-videostab2.4 libopencv-objdetect2.4 libopencv-calib3d2.4 "
echo -e "gap loi libopencv_objdetect.so: undefined reference opencv
	chon crosscompiler gcc aarch-__-gcc -v nho hon 6.0"
echo -e "tham khao link https://answers.opencv.org/question/199567/cross-compile-opencv-343-for-arm/"

#"error VIDIOC_REQBUFS: Inappropriate ioctl for device Tôi đã chỉnh sửa cmake/checks/ffmpeg_test.cpp và thêm #define __STDC_CONSTANT_MACROS vào đầu tệp, sau đó phát hiện FFMPEG ok với cmake, và biên dịch và chạy tốt"
#"error VIDIOC_REQBUFS: Inappropriate ioctl for device https://xbuba.com/questions/53504289


#Build OpenCV to Custom FFMPEG Install https://stackoverflow.com/questions/12427928/configure-and-build-opencv-to-custom-ffmpeg-install



