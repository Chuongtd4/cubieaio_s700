#! /bin/sh
#!/bin/bash
clear

echo -e " ____Install script for OpenCV on the host_____ "
echo -e " ---------------------------- "
echo
echo -e " \033[0;97;2:31;5m author  : chuongtd4@gmail.com\033[0m";
echo
echo -e " \033[39;31;2:39;5m date    : 09/09/2019\033[0m";

echo -e " ---------------------------- "
echo
echo -e " \033[1;36mDBG  \033[0m \033[0;90m=\033[0m \033[0;34m1\033[0m";
echo -e " \033[1;36mDBG_1\033[0m \033[0;90m=\033[0m \033[0;34m0\033[0m"
tput ed
tput sgr0
echo

echo "\nOpenCV Install Script for Ubuntu\n"

# Activate all the apt-get repositories
echo "\nAdding all apt-get repositories\n"
sudo add-apt-repository main
sudo add-apt-repository universe
sudo add-apt-repository multiverse
sudo add-apt-repository restricted
sudo apt-get update

# Install OpenCV dependencies
echo "\nInstalling dependencies\n"
sudo apt-get install git vim curl cmake g++ libopencv-dev build-essential libgtk2.0-dev pkg-config python-dev python-numpy libdc1394-22 libdc1394-22-dev libjpeg-dev libpng12-dev libtiff5 libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libxine2-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev libtbb-dev libqt4-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils unzip -y

# Install OpenCV 3.0.0
echo "\nPreparing for OpenCV installation\n"
cd ~/
mkdir -p ~/opencv.host
cd ~/opencv.host
git clone https://github.com/opencv/opencv.git
cd ~/opencv.host/opencv
git checkout 3.4
#!/bin/bash
clear

echo -e " ____compiler opencv for action s700_____ "
echo -e " ---------------------------- "
echo
echo -e " \033[0;97;2:31;5m author  : chuongtd4@gmail.com\033[0m";
echo
echo -e " \033[39;31;2:39;5m date    : 06/09/2019\033[0m";

echo -e " ---------------------------- "
echo
echo -e " \033[1;36mDBG  \033[0m \033[0;90m=\033[0m \033[0;34m1\033[0m";
echo -e " \033[1;36mDBG_1\033[0m \033[0;90m=\033[0m \033[0;34m0\033[0m"
tput ed
tput sgr0
echo

# Additional repository for SIFT and SURF patented operators
cd  ~/opencv.host/
git clone https://github.com/opencv/opencv_contrib.git
cd ~/opencv.host/opencv_contrib
git checkout 3.4

mkdir ~/opencv.host/opencv/build.host
cd    ~/opencv.host/opencv/build.host

echo "\nStarting OpenCV installation\n"


echo -e $(sudo cmake \
-D BUILD_TIFF=ON \
-D WITH_CmakeUDA=OFF \
-D ENABLE_AVX=OFF \
-D WITH_OPENGL=OFF \
-D WITH_OPENCL=OFF \
-D WITH_IPP=OFF \
-D WITH_TBB=ON \
-D BUILD_TBB=ON \
-D WITH_EIGEN=OFF \
-D WITH_V4L=ON \
-D WITH_VTK=ON \
-D WITH_GTK_2_X=ON \
-D BUILD_TESTS=OFF \
-D BUILD_PERF_TESTS=OFF \
-D CMAKE_BUILD_TYPE=RELEASE \
-D CMAKE_INSTALL_PREFIX=/usr/local \
-D OPENCV_EXTRA_MODULES_PATH=~/opencv.host/opencv_contrib/modules ~/opencv.host/opencv \
../
)

make -j4
sudo make install

#sudo /bin/bash -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig
pkg-config --cflags opencv
pkg-config --libs opencv

echo "\nDone installing OpenCV (may or may not have been successful)\n"
