#ifndef GPIO_H
#define GPIO_H
#include <common.h>
#include <errno.h>


#include <common.h>
#include <errno.h>

#define RET_OK				0
#define Disable 0
#define Enable  1
#define GPIO_MFP_PWM 0xE01B0000
#define GPIO_EINEN       *(unsigned int *)(GPIO_MFP_PWM + 0x0034) //set intput
#define GPIO_EOUTEN		*(unsigned int *)(GPIO_MFP_PWM + 0x0030)  //set output
#define GPIO_EDAT       *(unsigned int *)(GPIO_MFP_PWM + 0x0038)  //set state
#define uchar unsigned char
extern void *_memcpy (void *dest, const void *src, size_t len);
extern void ndelay(int delay);
void oled_i2c_init();
void close_porte();
static int i2c_gpio_write_byte(int delay, uchar data);
extern int i2c_gpio_write_data(uchar chip,uchar* buffer, int len,bool end_with_repeated_start);

static int i2c_gpio_set_bus_speed(unsigned int speed_hz);
#endif // GPIO_H

