#include "ssd1306.h"
#include "i2c_sofware.h"
#include <common.h>
#include <errno.h>

#include <stddef.h>



const uchar*  m_font;      // Current font.
static uint8_t m_font_offset = 2;  // Font bytes for meta data.
static uint8_t m_font_width;       // Font witdth.
static uint8_t m_col;              // Cursor column.
static uint8_t m_row;              // Cursor row (RAM).
static char addressingMode;



void SSD1306_exit()
{

    close_porte();
}

int __write(uchar reg,uchar* data, int leg)
{
    uchar buf[leg+2];

    buf[0]=reg;
    int i;
    for(i=0;i<leg;i++)
    {
        buf[i+1]=data[i];

    }

    int ret= i2c_gpio_write_data(SSD1306_Address,&buf,leg+1,0);
    return ret ;
}
void SSD1306_sendCommand(uchar command)
{

    if( __write(SSD1306_Command_Mode, &command, 1)<=0)
    {
        ;
    }
   // printf("ssd1306 send comment %d\n",command);
}
void SSD1306_sendData(unsigned char Data)
{

    if( __write(SSD1306_Data_Mode, &Data, 1)<=0)
    {
        //printf("I2C write error\n");
    }
}
int SSD1306_putChar(unsigned char ch)
{
    if (!m_font) return 0;
    //Ignore non-printable ASCII characters. This can be modified for
    //multilingual font.
    if(ch < 32 || ch > 127)
    {
        ch = ' ';
    }
    unsigned char i = 0;
    for(i=0;i<m_font_width;i++)
    {
        // Font array starts at 0, ASCII starts at 32
        SSD1306_sendData(pgm_read_byte(&m_font[(ch-32)*m_font_width+m_font_offset+i]));
    }
}
void SSD1306_putString(const char *string)
{
    unsigned char i=0;
    while(string[i])
    {
        SSD1306_putChar(string[i]);
        i++;
    }
}

void SSD1306_setTextXY(unsigned char row, unsigned char col)
{
    SSD1306_sendCommand(0xB0 + row);                          //set page address
    SSD1306_sendCommand(0x00 + (m_font_width*col & 0x0F));    //set column lower addr
    SSD1306_sendCommand(0x10 + ((m_font_width*col>>4)&0x0F)); //set column higher addr
}
void SSD1306_clearDisplay()
{
    unsigned char i,j;
    SSD1306_sendCommand(SSD1306_Display_Off_Cmd);     //display off
    for(j=0;j<8;j++)
    {
        SSD1306_setTextXY(j,0);
        {
            for(i=0;i<16;i++)  //clear all columns
            {
                SSD1306_putChar(' ');
            }
        }
    }
    SSD1306_sendCommand(SSD1306_Display_On_Cmd);     //display on
    SSD1306_setTextXY(0,0);
}
void SSD1306_setFont(const uint8_t* font)
{
    m_font = font;
    m_font_width = pgm_read_byte(&m_font[0]);
}

void SSD1306_setBrightness(unsigned char Brightness)
{
    SSD1306_sendCommand(SSD1306_Set_Brightness_Cmd);
    SSD1306_sendCommand(Brightness);
}
void SSD1306_setPageMode()
{
    addressingMode = PAGE_MODE;
    SSD1306_sendCommand(0x20);                      //set addressing mode
    SSD1306_sendCommand(0x02);                      //set page addressing mode
}
void SSD1306_setHorizontalMode()
{
    addressingMode = HORIZONTAL_MODE;
    SSD1306_sendCommand(0x20);                      //set addressing mode
    SSD1306_sendCommand(0x00);                      //set horizontal addressing mode
}
unsigned char SSD1306_putNumber(long long_num)
{
  unsigned char char_buffer[10]="";
  unsigned char i = 0;
  unsigned char f = 0;

  if (long_num < 0)
  {
    f=1;
    SSD1306_putChar('-');
    long_num = -long_num;
  }
  else if (long_num == 0)
  {
    f=1;
    SSD1306_putChar('0');
    return f;
  }

  while (long_num > 0)
  {
    char_buffer[i++] = long_num % 10;
    long_num /= 10;
  }

  f=f+i;
  for(; i > 0; i--)
  {
    SSD1306_putChar('0'+ char_buffer[i - 1]);
  }
  return f;

}


void SSD1306_drawBitmap(unsigned char *bitmaparray,int bytes)
{
  char localAddressMode = addressingMode;
  if(addressingMode != HORIZONTAL_MODE)
  {
      //Bitmap is drawn in horizontal mode
      SSD1306_setHorizontalMode();
  }
  int i;
  for(i=0;i<bytes;i++)
  {
      SSD1306_sendData(pgm_read_byte(&bitmaparray[i]));
  }

  if(localAddressMode == PAGE_MODE)
  {
     //If pageMode was used earlier, restore it.
     SSD1306_setPageMode();
  }

}

void SSD1306_init()
{
    oled_i2c_init();
    m_font_offset = 2;
    printf("***************SSD1306_init***************\n");
    SSD1306_sendCommand(0xAE);            //display off
    SSD1306_sendCommand(0xA6);            //Set Normal Display (default)
    SSD1306_sendCommand(0xAE);            //DISPLAYOFF
    SSD1306_sendCommand(0xD5);            //SETDISPLAYCLOCKDIV
    SSD1306_sendCommand(0x80);            // the suggested ratio 0x80
    SSD1306_sendCommand(0xA8);            //SSD1306_SETMULTIPLEX
    SSD1306_sendCommand(0x3F);
    SSD1306_sendCommand(0xD3);            //SETDISPLAYOFFSET
    SSD1306_sendCommand(0x0);             //no offset
    SSD1306_sendCommand(0x40|0x0);        //SETSTARTLINE
    SSD1306_sendCommand(0x8D);            //CHARGEPUMP
    SSD1306_sendCommand(0x14);
    SSD1306_sendCommand(0x20);            //MEMORYMODE
    SSD1306_sendCommand(0x00);            //0x0 act like ks0108
    SSD1306_sendCommand(0xA1);            //SEGREMAP   Mirror screen horizontally (A0)
    SSD1306_sendCommand(0xC8);            //COMSCANDEC Rotate screen vertically (C0)
    SSD1306_sendCommand(0xDA);            //0xDA
    SSD1306_sendCommand(0x12);            //COMSCANDEC
    SSD1306_sendCommand(0x81);            //SETCONTRAST
    SSD1306_sendCommand(0xCF);            //
    SSD1306_sendCommand(0xd9);            //SETPRECHARGE
    SSD1306_sendCommand(0xF1);
    SSD1306_sendCommand(0xDB);            //SETVCOMDETECT
    SSD1306_sendCommand(0x40);
    SSD1306_sendCommand(0xA4);            //DISPLAYALLON_RESUME
    SSD1306_sendCommand(0xA6);            //NORMALDISPLAY
    SSD1306_clearDisplay();
    SSD1306_sendCommand(0x2E);            //Stop scroll
    SSD1306_sendCommand(0x20);            //Set Memory Addressing Mode
    SSD1306_sendCommand(0x00);            //Set Memory Addressing Mode ab Horizontal addressing mode
    SSD1306_setFont(font8x8);

}


void blink_led()
{

    printf("****************blink_led ********************\n");


    SSD1306_init();
    SSD1306_setBrightness(255);
    SSD1306_setPageMode();

    SSD1306_clearDisplay();
    SSD1306_putString("test oled\n");
    SSD1306_clearDisplay();

        SSD1306_clearDisplay();
        printf("**\n");
        SSD1306_setBrightness(0);
        SSD1306_putString("1\n");
        mdelay(10);
        SSD1306_setBrightness(255);
        SSD1306_putString("test oled\n");
        mdelay(2000);

}
/***********ssd1306**************/
