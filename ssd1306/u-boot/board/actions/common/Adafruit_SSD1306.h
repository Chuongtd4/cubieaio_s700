/**
 * original author:  giatri<giatrispkt@gmail.com>
 * modification for STM32f10x: giatri<giatrispkt@gmail.com>

   ----------------------------------------------------------------------
   	Copyright (C) GiaTri, 2016
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.
     
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
   ----------------------------------------------------------------------
 */
#ifndef _Adafruit_SSD1306_H
#define _Adafruit_SSD1306_H 100

/* C++ detection */
#ifdef __cplusplus
extern C {
#endif

/**
 * This SSD1306 LCD uses I2C for communication
 *
 * Library features functions for drawing lines, rectangles and circles.
 *
 * It also allows you to draw texts and characters using appropriate functions provided in library.
 *
 * Default pinout
 *
SSD1306    |STM32F3xx    |DESCRIPTION

VCC        |3.3V         |
GND        |GND          |
SCL        |PB6          |Serial clock line
SDA        |PB7          |Serial data line
*/
/*=========================================================================
							Select Chip Version
=========================================================================*/

#include <stdbool.h>
#include <stdint.h>
#include "i2c_sofware.h"
#define BLACK 0
#define WHITE 1
#define INVERSE 2

#define SSD1306_I2C_ADDRESS  0x3c // 0x78
#define USE_OLED_I2C_DMA 0
//#define SSD1306_I2C_ADDRESS       0x7A
#if USE_OLED_I2C_DMA
typedef enum
{
  OLED_OK       = 0x00U,
  OLED_ERROR    = 0x01U,
  OLED_BUSY     = 0x02U,
  OLED_TIMEOUT  = 0x03U,
  OLED_CMD  = 0x04U	
} OLED_DMA_status;
//

//
#endif
/*=========================================================================
    SSD1306 Displays
    -----------------------------------------------------------------------
    The driver is used in multiple displays (128x64, 128x32, etc.).
    Select the appropriate display below to create an appropriately
    sized framebuffer, etc.

    SSD1306_128_64  128x64 pixel display

    SSD1306_128_32  128x32 pixel display

    SSD1306_96_16

    -----------------------------------------------------------------------*/
   #define SSD1306_128_64
   //#define SSD1306_128_32
//   #define SSD1306_96_16
/*=========================================================================*/

#if defined SSD1306_128_64 && defined SSD1306_128_32
  #error "Only one SSD1306 display can be specified at once in SSD1306.h"
#endif
#if !defined SSD1306_128_64 && !defined SSD1306_128_32 && !defined SSD1306_96_16
  #error "At least one SSD1306 display must be specified in SSD1306.h"
#endif

#if defined SSD1306_128_64
  #define SSD1306_LCDWIDTH                  128
  #define SSD1306_LCDHEIGHT                 64
#endif
#if defined SSD1306_128_32
  #define SSD1306_LCDWIDTH                  128
  #define SSD1306_LCDHEIGHT                 32
#endif
#if defined SSD1306_96_16
  #define SSD1306_LCDWIDTH                  96
  #define SSD1306_LCDHEIGHT                 16
#endif

#define SSD1306_SETCONTRAST 0x81
#define SSD1306_DISPLAYALLON_RESUME 0xA4
#define SSD1306_DISPLAYALLON 0xA5
#define SSD1306_NORMALDISPLAY 0xA6
#define SSD1306_INVERTDISPLAY 0xA7
#define SSD1306_DISPLAYOFF 0xAE//
#define SSD1306_DISPLAYON 0xAF//

#define SSD1306_SETDISPLAYOFFSET 0xD3//
#define SSD1306_SETCOMPINS 0xDA//

#define SSD1306_SETVCOMDETECT 0xDB//

#define SSD1306_SETDISPLAYCLOCKDIV 0xD5//
#define SSD1306_SETPRECHARGE 0xD9//

#define SSD1306_SETMULTIPLEX 0xA8//

#define SSD1306_SETLOWCOLUMN 0x00//
#define SSD1306_SETHIGHCOLUMN 0x10//

#define SSD1306_SETSTARTLINE 0x40//

#define SSD1306_MEMORYMODE 0x20//
#define SSD1306_COLUMNADDR 0x21
#define SSD1306_PAGEADDR   0x22

#define SSD1306_PAGESTARTADDR 0XB0 //Page Start Address

#define SSD1306_COMSCANINC 0xC0
#define SSD1306_COMSCANDEC 0xC8//

#define SSD1306_SEGREMAP 0xA0//

#define SSD1306_CHARGEPUMP 0x8D//

#define SSD1306_EXTERNALVCC 0x1////
#define SSD1306_SWITCHCAPVCC 0x2//

// Scrolling #defines
#define SSD1306_ACTIVATE_SCROLL 0x2F
#define SSD1306_DEACTIVATE_SCROLL 0x2E
#define SSD1306_SET_VERTICAL_SCROLL_AREA 0xA3
#define SSD1306_RIGHT_HORIZONTAL_SCROLL 0x26
#define SSD1306_LEFT_HORIZONTAL_SCROLL 0x27
#define SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29
#define SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL 0x2A
 /*--------------------------------------------------------------*/
 typedef enum
{ False = 0,
  True = !False
} boolean;

 typedef struct
{
	int32_t WIDTH, HEIGHT;			/* This is the 'raw' display w/h - never changes */
	int32_t _width, _height;		/* Display w/h as modified by current rotation */
	int32_t cursor_x, cursor_y;
	uint16_t textcolor, textbgcolor;
	uint8_t textsize;
	uint8_t rotation;
	boolean wrap;					/* If set, 'wrap' text at right edge of display */

} SSD1306_t;

	 void GFX_Init(void);
	 
	  
	 void begin(uint8_t switchvcc, uint8_t i2caddr);
     int  write_reg(uchar reg,uchar* data, int leg);
	 void ssd1306_command(uint8_t command);
	 
	 void drawCircle(int32_t x0, int32_t y0, int32_t r, uint16_t color);
	 void drawCircleHelper(int32_t x0, int32_t y0, int32_t r, uint8_t cornername, uint16_t color);
	 void fillCircle(int32_t x0, int32_t y0, int32_t r, uint16_t color);
	 void fillCircleHelper(int32_t x0, int32_t y0, int32_t r, uint8_t cornername, int32_t delta, uint16_t color);

	 void drawLine(int32_t x0, int32_t y0, int32_t x1, int32_t y1, uint16_t color);
	 void drawRect(int32_t x, int32_t y, int32_t w, int32_t h, uint16_t color);

	 void drawFastVLine(int32_t x, int32_t y, int32_t h, uint16_t color);
	 void drawFastHLine(int32_t x, int32_t y, int32_t w, uint16_t color);

	 void fillRect(int32_t x, int32_t y, int32_t w, int32_t h, uint16_t color);
	 void fillScreen(uint16_t color);

	 void drawRoundRect(int32_t x, int32_t y, int32_t w, int32_t h, int32_t r, uint16_t color);
	 void fillRoundRect(int32_t x, int32_t y, int32_t w, int32_t h, int32_t r, uint16_t color);

	 void drawTriangle(int32_t x0, int32_t y0, int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint16_t color);
	 void fillTriangle(int32_t x0, int32_t y0, int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint16_t color);
	 
	 void clearDisplay(void);
	 void invertDisplay(uint8_t i);
	 void display(void);

	 void startscrollright(uint8_t start, uint8_t stop);
	 void startscrollleft(uint8_t start, uint8_t stop);

	 void startscrolldiagright(uint8_t start, uint8_t stop);
	 void startscrolldiagleft(uint8_t start, uint8_t stop);
	 void stopscroll(void);

	 void dim(boolean dim);

	 void drawPixel(int32_t x, int32_t y, uint16_t color); 
   /*------------------------------------------------------------------------------------------------*/
	 void drawChar(int32_t x, int32_t y, unsigned char c, uint16_t color, uint16_t bg, uint8_t size);
	 
	 void write(uint8_t c); 
	 void print(const char *String);
	 void println(const char *String);
	 void printDigit(uint8_t Digit);
	 void printDigitln(uint8_t Digit);
	 void printHex(uint8_t Byte, boolean Prefix);
	 void printHexln(uint8_t Byte, boolean Prefix);
	 void printNumber(int32_t Number, boolean Spaces);
	 void printNumberln(int32_t Number, boolean Spaces);
   
	 /*------------------------------------------------------------------------------------------------*/
	 void drawBitmap(int32_t x, int32_t y, uint8_t *bitmap, int32_t w, int32_t h, uint16_t color);
     void drawBitmapBg(int32_t x, int32_t y, uint8_t *bitmap, int32_t w, int32_t h, uint16_t color, uint16_t bg);
	 
	 void setCursor(int32_t x, int32_t y);
	 void setTextSize(uint8_t s);
	 void setTextColor(uint16_t c);
	 void setTextColorAndBackground(uint16_t c, uint16_t b);
	 
	 uint8_t getRotation(void);
	 void setRotation(uint8_t x);
	 
	 int32_t get_width(void);
	 int32_t get_height(void);
	 
	 int32_t getCursorX(void);
	 int32_t getCursorY(void);
	 uint8_t gettextsize (void);
	 void setContrast(uint8_t contrast);
	 
	 //============================= SCREEN LARGER =============================================================//
 /* C++ detection */
 /*************************************Binh Trong ***************************************/
 /********************Add some functions 02/08/2018***************************************/
 #define RADS 0.017453293
	void printFromPoint(const char *String,uint16_t point);
	void drawBitmapCircle(int32_t x, int32_t y, uint8_t *bitmap, int32_t w, int32_t h, int32_t r, uint16_t color);
	void drawBitmapLimit_X(int32_t x, int32_t y, uint8_t *bitmap, int32_t w, int32_t h, 
											 int32_t limitDown, int32_t limitUp, uint16_t color);
	void drawBitmapLimit_Y(int32_t x, int32_t y, uint8_t *bitmap, int32_t w, int32_t h, 
											 int32_t limitDown, int32_t limitUp, uint16_t color);
	void drawARC(int32_t x, int32_t y, int32_t r,uint16_t color, int32_t startAngle, int32_t endAngle,int32_t disAngle,int32_t rc,uint8_t typeDraw);
	void print_limit(const char *String, int x_limit, int y_limit, uint8_t direct_limit);
#ifdef __cplusplus
}
#endif
#endif




