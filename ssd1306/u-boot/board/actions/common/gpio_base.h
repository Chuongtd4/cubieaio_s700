#ifndef GPIO_BASE_H
#define GPIO_BASE_H

#include <common.h>
#include <errno.h>
#define GPIOD_MFP_PWM  0xE01B0000
#define GPIO_DINEN      *(unsigned int *)(GPIOD_MFP_PWM + 0x0028) //set intput
#define GPIO_DOUTEN		*(unsigned int *)(GPIOD_MFP_PWM + 0x0024)  //set output
#define GPIO_DDAT       *(unsigned int *)(GPIOD_MFP_PWM + 0x002C)  //set state

#define GPIO_AINEN      *(unsigned int *)(GPIOD_MFP_PWM + 0x0004) //set intput
#define GPIO_AOUTEN		*(unsigned int *)(GPIOD_MFP_PWM + 0x0000)  //set output
#define GPIO_ADAT       *(unsigned int *)(GPIOD_MFP_PWM + 0x0008)  //set state

enum {
    INPUT=0,
    OUTPUT=1
};
enum {
    SET=0,
    RESET=1
};
void gpio_set(int port,int pin);
void gpio_clear(int port,int pin);
int  gpio_get(int port,int pin);
void setmode(int reg, int pin,int mode );
#endif // GPIO_BASE_H
