#include "Adafruit_SSD1306.h"
#include "glcdfont.c"
#include "stdlib.h"
#include <stddef.h>

/*-----------------------------------------------------------------*/
	#if USE_OLED_I2C_DMA	
volatile uint8_t Flag_Enable_I2C_DMA=OLED_OK;
uint8_t bufferDMA[SSD1306_LCDHEIGHT * SSD1306_LCDWIDTH / 8];
volatile static uint8_t mDMApage=255;
volatile	static uint8_t cmdOled=0;
	#endif
//
uint8_t buffer[SSD1306_LCDHEIGHT * SSD1306_LCDWIDTH / 8];

#define ssd1306_swap(a, b) { int32_t t = a; a = b; b = t; }

SSD1306_t SSD1306;
uint8_t _i2caddr, _vccstate;

void GFX_Init(void){
	
	SSD1306.WIDTH = SSD1306_LCDWIDTH;
	SSD1306.HEIGHT = SSD1306_LCDHEIGHT;
	SSD1306._width = SSD1306.WIDTH;
	SSD1306._height = SSD1306.HEIGHT;
	
	SSD1306.rotation  = 2;
	SSD1306.cursor_y  = SSD1306.cursor_x = 0;
	SSD1306.textsize = 1;
	SSD1306.textcolor = SSD1306.textbgcolor = 0xFFFF;
    //SSD1306.wrap      = True;

}
void drawPixel(int32_t x, int32_t y, uint16_t color)
{
  if ((x < 0) || (x >= SSD1306_LCDWIDTH) || (y < 0) || (y >= SSD1306_LCDHEIGHT))
	return;

  // check rotation, move pixel around if necessary
  switch (getRotation()) {
  case 1:
    ssd1306_swap(x, y);
    x = SSD1306.WIDTH - x - 1;
    break;
  case 2:
    x = SSD1306.WIDTH - x - 1;
    y = SSD1306.HEIGHT - y - 1;
    break;
  case 3:
    ssd1306_swap(x, y);
    y = SSD1306.HEIGHT - y - 1;
    break;
  }

  // x is which column
    switch (color)
    {
      case WHITE:   buffer[x+ (y/8)*SSD1306_LCDWIDTH] |=  (1 << (y&7)); break;
      case BLACK:   buffer[x+ (y/8)*SSD1306_LCDWIDTH] &= ~(1 << (y&7)); break;
      case INVERSE: buffer[x+ (y/8)*SSD1306_LCDWIDTH] ^=  (1 << (y&7)); break;
	  }
		
} 

void begin(uint8_t vccstate, uint8_t i2caddr) 
{
   oled_i2c_init();
  _vccstate = vccstate;
  _i2caddr = i2caddr;
  // Init sequence
  ssd1306_command(SSD1306_DISPLAYOFF);                    // 0xAE
  ssd1306_command(SSD1306_SETDISPLAYCLOCKDIV);            // 0xD5
  ssd1306_command(0x80);                                  // the suggested ratio 0x80

  ssd1306_command(SSD1306_SETMULTIPLEX);                  // 0xA8
  ssd1306_command(SSD1306_LCDHEIGHT - 1);

  ssd1306_command(SSD1306_SETDISPLAYOFFSET);              // 0xD3
  ssd1306_command(0x0);                                   // no offset
  ssd1306_command(SSD1306_SETSTARTLINE | 0x0);            // line #0
  ssd1306_command(SSD1306_CHARGEPUMP);                    // 0x8D
  if (vccstate == SSD1306_EXTERNALVCC)
    { ssd1306_command(0x10); }
  else
    { ssd1306_command(0x14); }
  ssd1306_command(SSD1306_MEMORYMODE);                    // 0x20
  ssd1306_command(0x00);                                  // 0x0 act like ks0108
  ssd1306_command(SSD1306_SEGREMAP | 0x1);
  ssd1306_command(SSD1306_COMSCANDEC);                    //0xC8

 #if defined SSD1306_128_32
  ssd1306_command(SSD1306_SETCOMPINS);                    // 0xDA
  ssd1306_command(0x02);
  ssd1306_command(SSD1306_SETCONTRAST);                   // 0x81
  ssd1306_command(0x8F);

#elif defined SSD1306_128_64
  ssd1306_command(SSD1306_SETCOMPINS);                    // 0xDA
  ssd1306_command(0x12);
  ssd1306_command(SSD1306_SETCONTRAST);                   // 0x81
  if (vccstate == SSD1306_EXTERNALVCC)
    { ssd1306_command(0x9F); }
  else
    { ssd1306_command(0xFF); }

#elif defined SSD1306_96_16
  ssd1306_command(SSD1306_SETCOMPINS);                    // 0xDA
  ssd1306_command(0x2);   //ada x12
  ssd1306_command(SSD1306_SETCONTRAST);                   // 0x81
  if (vccstate == SSD1306_EXTERNALVCC)
    { ssd1306_command(0x10); }
  else
    { ssd1306_command(0xAF); }

#endif

  ssd1306_command(SSD1306_SETPRECHARGE);                  // 0xd9
  if (SSD1306_SETSTARTLINE == SSD1306_EXTERNALVCC)
    { ssd1306_command(0x22); }
  else
    { ssd1306_command(0xF1); }
  ssd1306_command(SSD1306_SETVCOMDETECT);                 // 0xDB
  ssd1306_command(0x40);
  ssd1306_command(SSD1306_DISPLAYALLON_RESUME);           // 0xA4
  ssd1306_command(SSD1306_NORMALDISPLAY);                 // 0xA6

  ssd1306_command(SSD1306_DEACTIVATE_SCROLL);

  ssd1306_command(SSD1306_DISPLAYON);//--turn on oled panel

  GFX_Init();

}
int write_reg(uchar reg,uchar* data, int leg)
{
    uchar buf[leg+2];
    buf[0]=reg;
    int i;
    for(i=0;i<leg;i++)
    {
        buf[i+1]=data[i];
    }

    leg=leg+1;
    int ret= i2c_gpio_write_data(SSD1306_I2C_ADDRESS,buf,leg,0);
    return ret ;
}


void ssd1306_command(uint8_t command)
{
    if( write_reg(0x80, &command, 1)<=0)
    {
            ;
    }
  //  HAL_I2C_Mem_Write(&hi2c2,SSD1306_I2C_ADDRESS,0x00,1,&command,1,10);


}
/*------------------------------ GUI --------------------------------*/

void drawCircle(int32_t x0, int32_t y0, int32_t r, uint16_t color)
{
	int32_t f = 1 - r;
	int32_t ddF_x = 1;
	int32_t ddF_y = -2 * r;
	int32_t x = 0;
	int32_t y = r;

	drawPixel(x0  , y0+r, color);
	drawPixel(x0  , y0-r, color);
	drawPixel(x0+r, y0  , color);
	drawPixel(x0-r, y0  , color);

	while (x < y)
	{
		if (f >= 0)
		{
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		drawPixel(x0 + x, y0 + y, color);
		drawPixel(x0 - x, y0 + y, color);
		drawPixel(x0 + x, y0 - y, color);
		drawPixel(x0 - x, y0 - y, color);
		drawPixel(x0 + y, y0 + x, color);
		drawPixel(x0 - y, y0 + x, color);
		drawPixel(x0 + y, y0 - x, color);
		drawPixel(x0 - y, y0 - x, color);
	}
}
void drawCircleHelper(int32_t x0, int32_t y0, int32_t r, uint8_t cornername, uint16_t color)
{
	int32_t f     = 1 - r;
	int32_t ddF_x = 1;
	int32_t ddF_y = -2 * r;
	int32_t x     = 0;
	int32_t y     = r;

	while (x < y)
	{
		if (f >= 0)
		{
			y--;
			ddF_y += 2;
			f     += ddF_y;
		}
		x++;
		ddF_x += 2;
		f     += ddF_x;
		if (cornername & 0x4)
		{
			drawPixel(x0 + x, y0 + y, color);
			drawPixel(x0 + y, y0 + x, color);
		}
		if (cornername & 0x2)
		{
			drawPixel(x0 + x, y0 - y, color);
			drawPixel(x0 + y, y0 - x, color);
		}
		if (cornername & 0x8)
		{
			drawPixel(x0 - y, y0 + x, color);
			drawPixel(x0 - x, y0 + y, color);
		}
		if (cornername & 0x1)
		{
			drawPixel(x0 - y, y0 - x, color);
			drawPixel(x0 - x, y0 - y, color);
		}
	}
}
void fillCircle(int32_t x0, int32_t y0, int32_t r, uint16_t color)
{
	drawFastVLine(x0, y0-r, 2*r+1, color);
	fillCircleHelper(x0, y0, r, 3, 0, color);
}
void fillCircleHelper(int32_t x0, int32_t y0, int32_t r, uint8_t cornername, int32_t delta, uint16_t color)
{
	int32_t f     = 1 - r;
	int32_t ddF_x = 1;
	int32_t ddF_y = -2 * r;
	int32_t x     = 0;
	int32_t y     = r;

	while (x < y)
	{
		if (f >= 0)
		{
			y--;
			ddF_y += 2;
			f     += ddF_y;
		}
		x++;
		ddF_x += 2;
		f     += ddF_x;

		if (cornername & 0x1)
		{
			drawFastVLine(x0+x, y0-y, 2*y+1+delta, color);
			drawFastVLine(x0+y, y0-x, 2*x+1+delta, color);
		}
		if (cornername & 0x2)
		{
			drawFastVLine(x0-x, y0-y, 2*y+1+delta, color);
			drawFastVLine(x0-y, y0-x, 2*x+1+delta, color);
		}
	}
}
void drawLine(int32_t x0, int32_t y0, int32_t x1, int32_t y1, uint16_t color)
{
	int16_t dx, dy, sx, sy, err, e2, i, tmp; 
	
	/* Check for overflow */
	if (x0 >= SSD1306_LCDWIDTH) {
		x0 = SSD1306_LCDWIDTH - 1;
	}
	if (x1 >= SSD1306_LCDWIDTH) {
		x1 = SSD1306_LCDWIDTH - 1;
	}
	if (y0 >= SSD1306_LCDHEIGHT) {
		y0 = SSD1306_LCDHEIGHT - 1;
	}
	if (y1 >= SSD1306_LCDHEIGHT) {
		y1 = SSD1306_LCDHEIGHT - 1;
	}
	
	dx = (x0 < x1) ? (x1 - x0) : (x0 - x1); 
	dy = (y0 < y1) ? (y1 - y0) : (y0 - y1); 
	sx = (x0 < x1) ? 1 : -1; 
	sy = (y0 < y1) ? 1 : -1; 
	err = ((dx > dy) ? dx : -dy) / 2; 

	if (dx == 0) {
		if (y1 < y0) {
			tmp = y1;
			y1 = y0;
			y0 = tmp;
		}
		
		if (x1 < x0) {
			tmp = x1;
			x1 = x0;
			x0 = tmp;
		}
		
		/* Vertical line */
		for (i = y0; i <= y1; i++) {
			drawPixel(x0, i, color);
		}
		
		/* Return from function */
		return;
	}
	
	if (dy == 0) {
		if (y1 < y0) {
			tmp = y1;
			y1 = y0;
			y0 = tmp;
		}
		
		if (x1 < x0) {
			tmp = x1;
			x1 = x0;
			x0 = tmp;
		}
		
		/* Horizontal line */
		for (i = x0; i <= x1; i++) {
			drawPixel(i, y0, color);
		}
		
		/* Return from function */
		return;
	}
	
	while (1) {
		drawPixel(x0, y0, color);
		if (x0 == x1 && y0 == y1) {
			break;
		}
		e2 = err; 
		if (e2 > -dx) {
			err -= dy;
			x0 += sx;
		} 
		if (e2 < dy) {
			err += dx;
			y0 += sy;
		} 
	}
}
	
void drawRect(int32_t x, int32_t y, int32_t w, int32_t h, uint16_t color)
{
	drawFastHLine(x, y, w, color);
	drawFastHLine(x, y+h-1, w, color);
	drawFastVLine(x, y, h, color);
	drawFastVLine(x+w-1, y, h, color);
}

void drawFastVLine(int32_t x, int32_t y, int32_t h, uint16_t color)
{
	drawLine(x, y, x, y+h-1, color);
}
void drawFastHLine(int32_t x, int32_t y, int32_t w, uint16_t color)
{
	drawLine(x, y, x+w-1, y, color);
}

void fillRect(int32_t x, int32_t y, int32_t w, int32_t h, uint16_t color)
{
    int32_t i;  for ( i=x; i<x+w; i++)
	{
		drawFastVLine(i, y, h, color);
	}
}
void fillScreen(uint16_t color)
{
	fillRect(0, 0, SSD1306._width, SSD1306._height, color);
}

void drawRoundRect(int32_t x, int32_t y, int32_t w, int32_t h, int32_t r, uint16_t color)
{
	/* smarter version */
	drawFastHLine( x+r  , y    , w-2*r, color); // Top
	drawFastHLine( x+r  , y+h-1, w-2*r, color); // Bottom
	drawFastVLine( x    , y+r  , h-2*r, color); // Left
	drawFastVLine( x+w-1, y+r  , h-2*r, color); // Right
	/* draw four corners */
	drawCircleHelper( x+r    , y+r    , r, 1, color);
	drawCircleHelper( x+w-r-1, y+r    , r, 2, color);
	drawCircleHelper( x+w-r-1, y+h-r-1, r, 4, color);
	drawCircleHelper( x+r    , y+h-r-1, r, 8, color);
}
void fillRoundRect(int32_t x, int32_t y, int32_t w, int32_t h, int32_t r, uint16_t color)
{
	// smarter version
	fillRect(x+r, y, w-2*r, h, color);

	// draw four corners
	fillCircleHelper(x+w-r-1, y+r, r, 1, h-2*r-1, color);
	fillCircleHelper(x+r    , y+r, r, 2, h-2*r-1, color);
}

void drawTriangle(int32_t x0, int32_t y0, int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint16_t color)
{
	drawLine(x0, y0, x1, y1, color);
	drawLine(x1, y1, x2, y2, color);
	drawLine(x2, y2, x0, y0, color);
}
void fillTriangle(int32_t x0, int32_t y0, int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint16_t color)
{
	int32_t a, b, y, last;

	// Sort coordinates by Y order (y2 >= y1 >= y0)
	if (y0 > y1) {
		ssd1306_swap(y0, y1); ssd1306_swap(x0, x1);
	}
	if (y1 > y2) {
		ssd1306_swap(y2, y1); ssd1306_swap(x2, x1);
	}
	if (y0 > y1) {
		ssd1306_swap(y0, y1); ssd1306_swap(x0, x1);
	}

	if (y0 == y2) { // Handle awkward all-on-same-line case as its own thing
		a = b = x0;
		if(x1 < a)      a = x1;
		else if(x1 > b) b = x1;
		if(x2 < a)      a = x2;
		else if(x2 > b) b = x2;
		drawFastHLine(a, y0, b-a+1, color);
		return;
	}

	int32_t
	dx01 = x1 - x0,
	dy01 = y1 - y0,
	dx02 = x2 - x0,
	dy02 = y2 - y0,
	dx12 = x2 - x1,
	dy12 = y2 - y1,
	sa   = 0,
	sb   = 0;

	// For upper part of triangle, find scanline crossings for segments
	// 0-1 and 0-2.  If y1=y2 (flat-bottomed triangle), the scanline y1
	// is included here (and second loop will be skipped, avoiding a /0
	// error there), otherwise scanline y1 is skipped here and handled
	// in the second loop...which also avoids a /0 error here if y0=y1
	// (flat-topped triangle).
	if (y1 == y2) last = y1;   // Include y1 scanline
	else         last = y1-1; // Skip it

	for (y=y0; y<=last; y++) {
		a   = x0 + sa / dy01;
		b   = x0 + sb / dy02;
		sa += dx01;
		sb += dx02;
		/* longhand:
		a = x0 + (x1 - x0) * (y - y0) / (y1 - y0);
		b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
		*/
		if (a > b) ssd1306_swap(a,b);
		drawFastHLine(a, y, b-a+1, color);
	}

	// For lower part of triangle, find scanline crossings for segments
	// 0-2 and 1-2.  This loop is skipped if y1=y2.
	sa = dx12 * (y - y1);
	sb = dx02 * (y - y0);
	for (; y<=y2; y++) {
		a   = x1 + sa / dy12;
		b   = x0 + sb / dy02;
		sa += dx12;
		sb += dx02;
		/* longhand:
		a = x1 + (x2 - x1) * (y - y1) / (y2 - y1);
		b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
		*/
		if (a > b) ssd1306_swap(a,b);
		drawFastHLine(a, y, b-a+1, color);
	}
}

void drawBitmap(int32_t x, int32_t y, uint8_t *bitmap, int32_t w, int32_t h, uint16_t color)
{
	  int32_t i, j, byteWidth = (w + 7) / 8;
    uint8_t byte = 0;

    for(j=0; j<h; j++) {
        for(i=0; i<w; i++ ) {
            if(i & 7) byte <<= 1;
            else      byte   = bitmap[j * byteWidth + i / 8];
            if(byte & 0x80) drawPixel(x+i, y+j, color);
        }
    }
}

void drawBitmapBg(int32_t x, int32_t y, uint8_t *bitmap, int32_t w, int32_t h, uint16_t color, uint16_t bg)
{
		int32_t i, j, byteWidth = (w + 7) / 8;
    uint8_t byte = 0;

    for(j=0; j<h; j++) {
        for(i=0; i<w; i++ ) {
            if(i & 7) byte <<= 1;
            else      byte   = bitmap[j * byteWidth + i / 8];
            if(byte & 0x80) drawPixel(x+i, y+j, color);
            else            drawPixel(x+i, y+j, bg);
        }
    }
}
/*------------------------------ print --------------------------------*/
void drawChar(int32_t x, int32_t y, unsigned char c, uint16_t color, uint16_t bg, uint8_t size)
{
	if ((x >= SSD1306._width)		||	// Clip right
	   (y >= SSD1306._height)		||	// Clip bottom
	   ((x + 6 * size - 1) < 0)		||	// Clip left
	   ((y + 8 * size - 1) < 0))		// Clip top
		return;

    int8_t i; for ( i=0; i<6; i++ )
	{
		uint8_t line;
		if (i == 5)
			line = 0x0;//space
		else
			// TODO: was progmem here before so test if this actually works!
			line = font[(c*5)+i];
        int8_t j; for ( j = 0; j<8; j++)
		{
			if (line & 0x1) //1
			{
				if (size == 1) // default size
					drawPixel(x+i, y+j, color);
				else {  // big size
					fillRect(x+(i*size), y+(j*size), size, size, color);
				}
			}
			else if (bg != color)
			{
				if (size == 1) // default size
					drawPixel(x+i, y+j, bg);
				else {  // big size
					fillRect(x+i*size, y+j*size, size, size, bg);
				}
			}
			line >>= 1;
		}
	}
}

void write(uint8_t c)
{
	if (c == '\n')
	{
		SSD1306.cursor_y += SSD1306.textsize*8;
		SSD1306.cursor_x  = 0;
	}
	else if (c == '\r') {
	// skip me
	}
	else
	{
		drawChar(SSD1306.cursor_x, SSD1306.cursor_y, c, SSD1306.textcolor, SSD1306.textbgcolor, SSD1306.textsize);
		SSD1306.cursor_x += SSD1306.textsize*6;
		if (SSD1306.wrap && (SSD1306.cursor_x > (SSD1306._width - SSD1306.textsize*6)))
		{
			SSD1306.cursor_y += SSD1306.textsize*8;
			SSD1306.cursor_x = 0;
		}
	}
}
void print(const char *String)
{
	while (*String != 0x00)
	{
		write(*String);
		String++;
	}
}
void println(const char *String)
{
	print(String);
	print("\n");
}
void printDigit(uint8_t Digit)
{
	if (Digit < 10)
	{
		uint8_t character = 48 + Digit;
		write(character);
	}
	else if (Digit < 16)
	{
		uint8_t character = 55 + Digit;
		write(character);
	}
}
void printDigitln(uint8_t Digit)
{
	printDigit(Digit);
	print("\n");
}
void printHex(uint8_t Byte, boolean Prefix)
{
	if (Prefix) print("0x");
	printDigit((Byte >> 4) & 0xF);
	printDigit(Byte & 0xF);
}
void printHexln(uint8_t Byte, boolean Prefix)
{
	printHex(Byte, Prefix);
	print("\n");
}
void printNumber(int32_t Number, boolean Spaces)
{
	uint8_t ones, tens, hundreds, thousands, tenThousands, hundredThousands, millions, tenMillions, hundredMillions, billions;
  if (Number < 0) print("-");
	Number =abs(Number);
	billions = Number / 1000000000;
	Number = Number % 1000000000;

	hundredMillions = Number / 100000000;
	Number = Number % 100000000;

	tenMillions = Number / 10000000;
	Number = Number % 10000000;

	millions = Number / 1000000;
	Number = Number % 1000000;

	hundredThousands = Number / 100000;
	Number = Number % 100000;

	tenThousands = Number / 10000;
	Number = Number % 10000;

	thousands = Number / 1000;
	Number = Number % 1000;

	hundreds = Number / 100;
	Number = Number % 100;

	tens = Number / 10;
	Number = Number % 10;

	ones = Number;

	if (billions)
	{
		printDigit((uint8_t)billions);
		if (Spaces) print(" ");
	}
	if (hundredMillions || billions)
	{
		printDigit((uint8_t)hundredMillions);
	}
	if (tenMillions || hundredMillions || billions)
	{
		printDigit((uint8_t)tenMillions);
	}
	if (millions || tenMillions || hundredMillions || billions)
	{
		printDigit((uint8_t)millions);
		if (Spaces) print(" ");
	}
	if (hundredThousands || millions || tenMillions || hundredMillions || billions)
	{
		printDigit((uint8_t)hundredThousands);
	}
	if (tenThousands || hundredThousands || millions || tenMillions || hundredMillions || billions)
	{
		printDigit((uint8_t)tenThousands);
	}
	if (thousands || tenThousands || hundredThousands || millions || tenMillions || hundredMillions || billions)
	{
		printDigit((uint8_t)thousands);
		if (Spaces) print(" ");
	}
	if (hundreds || thousands || tenThousands || hundredThousands || millions || tenMillions || hundredMillions || billions)
	{
		printDigit((uint8_t)hundreds);
	}
	if (tens || hundreds || thousands || tenThousands || hundredThousands || millions || tenMillions || hundredMillions || billions)
	{
		printDigit((uint8_t)tens);
	}
	printDigit((uint8_t)ones);
}
void printNumberln(int32_t Number, boolean Spaces)
{
	printNumber(Number, Spaces);
	print("\n");
}
/*--------------------------- control oled ----------------------------*/

//
 void clearDisplay(void)
 {
		memset(buffer, 0, (SSD1306_LCDWIDTH*SSD1306_LCDHEIGHT/8));
 }	 
 void invertDisplay(uint8_t i)
 {
	if (i) {
    ssd1306_command(SSD1306_INVERTDISPLAY);
    } 
	else {
		ssd1306_command(SSD1306_NORMALDISPLAY);
	}
 } 
 //
 //

void Oled_EV_IRQHandler(void){
#if USE_OLED_I2C_DMA	
	 	uint8_t endpage;
  #if SSD1306_LCDHEIGHT == 64
    endpage = 8; // Page end address
  #endif
  #if SSD1306_LCDHEIGHT == 32
    endpage = 4; // Page end address
  #endif
  #if SSD1306_LCDHEIGHT == 16
    endpage = 2; // Page end address
  #endif
	if(Flag_Enable_I2C_DMA!=OLED_CMD){
uint8_t command;
if(mDMApage<endpage){		
switch(cmdOled){
	case 0:
command=0xB0 + mDMApage;	
HAL_I2C_Mem_Write_DMA(&hi2c1,SSD1306_I2C_ADDRESS,0x00,1,&command,1);			
	cmdOled=1;
	break;
	case 1:
command=0x00;
	HAL_I2C_Mem_Write_DMA(&hi2c1,SSD1306_I2C_ADDRESS,0x00,1,&command,1);	
	cmdOled=2;
	break;
	case 2:
	command=0x10;	
	HAL_I2C_Mem_Write_DMA(&hi2c1,SSD1306_I2C_ADDRESS,0x00,1,&command,1);	
	cmdOled=3;	
	break;
	case 3:		
HAL_I2C_Mem_Write_DMA(&hi2c1,SSD1306_I2C_ADDRESS,0x40,1,&bufferDMA[SSD1306_LCDWIDTH * mDMApage],SSD1306_LCDWIDTH);
mDMApage++;
cmdOled=0;	
break;		
}
}
else if(mDMApage==endpage)mDMApage++;
}
else Flag_Enable_I2C_DMA=OLED_OK;	
#endif
}
 //
 void
 display()
 {
	uint8_t endpage;
  #if SSD1306_LCDHEIGHT == 64
    endpage = 8; // Page end address
  #endif
  #if SSD1306_LCDHEIGHT == 32
    endpage = 4; // Page end address
  #endif
  #if SSD1306_LCDHEIGHT == 16
    endpage = 2; // Page end address
  #endif
	 	#if USE_OLED_I2C_DMA
if(mDMApage>endpage){
mDMApage=0;
cmdOled=1;
uint8_t command=0xB0 + mDMApage;	
memcpy(bufferDMA,buffer,sizeof(buffer));		
HAL_I2C_Mem_Write_DMA(&hi2c1,SSD1306_I2C_ADDRESS,0x00,1,&command,1);
}
            #else
    uint8_t mpage;
    for (mpage = 0; mpage < endpage; mpage++) {
	ssd1306_command(0xB0 + mpage);									
	ssd1306_command(0x00);
	ssd1306_command(0x10);
//chuongtd4 edit	HAL_I2C_Mem_Write(&hi2c2,SSD1306_I2C_ADDRESS,0x40,1,&buffer[SSD1306_LCDWIDTH * mpage],SSD1306_LCDWIDTH,10);
     write_reg(0x40,&buffer[SSD1306_LCDWIDTH * mpage],SSD1306_LCDWIDTH);
	}
	  #endif		
 }
 
// startscrollright
// Activate a right handed scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display.scrollright(0x00, 0x0F)
void startscrollright(uint8_t start, uint8_t stop)
	{
  ssd1306_command(SSD1306_RIGHT_HORIZONTAL_SCROLL);
  ssd1306_command(0X00);
  ssd1306_command(start);
  ssd1306_command(0X00);
  ssd1306_command(stop);
  ssd1306_command(0X00);
  ssd1306_command(0XFF);
  ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

// startscrollleft
// Activate a right handed scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display.scrollright(0x00, 0x0F)
void startscrollleft(uint8_t start, uint8_t stop)
{
  ssd1306_command(SSD1306_LEFT_HORIZONTAL_SCROLL);
  ssd1306_command(0X00);
  ssd1306_command(start);
  ssd1306_command(0X00);
  ssd1306_command(stop);
  ssd1306_command(0X00);
  ssd1306_command(0XFF);
  ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

// startscrolldiagright
// Activate a diagonal scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display.scrollright(0x00, 0x0F)
void startscrolldiagright(uint8_t start, uint8_t stop)
	{
  ssd1306_command(SSD1306_SET_VERTICAL_SCROLL_AREA);
  ssd1306_command(0X00);
  ssd1306_command(SSD1306_LCDHEIGHT);
  ssd1306_command(SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL);
  ssd1306_command(0X00);
  ssd1306_command(start);
  ssd1306_command(0X00);
  ssd1306_command(stop);
  ssd1306_command(0X01);
  ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

// startscrolldiagleft
// Activate a diagonal scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display.scrollright(0x00, 0x0F)
void startscrolldiagleft(uint8_t start, uint8_t stop){
  ssd1306_command(SSD1306_SET_VERTICAL_SCROLL_AREA);
  ssd1306_command(0X00);
  ssd1306_command(SSD1306_LCDHEIGHT);
  ssd1306_command(SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL);
  ssd1306_command(0X00);
  ssd1306_command(start);
  ssd1306_command(0X00);
  ssd1306_command(stop);
  ssd1306_command(0X01);
  ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

void stopscroll(void){
  ssd1306_command(SSD1306_DEACTIVATE_SCROLL);
}

// Dim the display
// dim = true: display is dimmed
// dim = false: display is normal

void dim(boolean dim) {
  uint8_t contrast;

  if (dim) {
    contrast = 0; // Dimmed display
  } else {
    if (_vccstate == SSD1306_EXTERNALVCC) {
      contrast = 0x9F;
    } else {
      contrast = 0xCF;
    }
  }
  // the range of contrast to too small to be really useful
  // it is useful to dim the display
  ssd1306_command(SSD1306_SETCONTRAST);
  ssd1306_command(contrast);
}

void setCursor(int32_t x, int32_t y)
{
	SSD1306.cursor_x = x;
	SSD1306.cursor_y = y;
}
void setTextSize(uint8_t s)
{
	SSD1306.textsize = (s > 0) ? s : 1;
}
void setTextColor(uint16_t c)
{
	SSD1306.textcolor = SSD1306.textbgcolor = c;
}
void setTextColorAndBackground(uint16_t c, uint16_t b)
{
	SSD1306.textcolor   = c;
	SSD1306.textbgcolor = b;
}

uint8_t getRotation()
{
	return SSD1306.rotation;
}

void setRotation(uint8_t x)
{
	SSD1306.rotation = (x & 3);
	switch (SSD1306.rotation) {
		case 0:
		case 2:
			SSD1306._width  = SSD1306.WIDTH;
			SSD1306._height = SSD1306.HEIGHT;
			break;
		case 1:
		case 3:
			SSD1306._width  = SSD1306.HEIGHT;
			SSD1306._height = SSD1306.WIDTH;
			break;
	}
}

int32_t get_width()
{
	return SSD1306._width;
}
int32_t get_height()
{
	return SSD1306._height;
}
int32_t getCursorX(void) {
  return SSD1306.cursor_x;
}

int32_t getCursorY(void) {
  return SSD1306.cursor_y;
}

uint8_t  gettextsize(void){
return SSD1306.textsize ;
}
void setContrast(uint8_t contrast)
{
	ssd1306_command(SSD1306_SETCONTRAST);                   // 0x81
  ssd1306_command(contrast); 
}

//========================== SCREEN LARGER =============================//


//===============================  END SCREEN LARGER  ==================================// 
/*************************************Binh Trong ***************************************/

//========================== SCREEN LARGER =============================//

void drawbitmap_roll(const uint8_t *bitmap,uint8_t col_max,int16_t limit_x1, int16_t limit_x2,int16_t  limit_y1,
	int16_t  limit_y2, uint16_t min_x,uint16_t max_x,uint16_t min_y ,uint16_t max_y,int16_t tran_x,int16_t tran_y,bool roll)
	{
		uint8_t tem=0;
		int16_t _x,_y,pix_x=0,pix_y=0;
		bool STATE_X=false,STATE_Y=false;
        int16_t j;for(j=min_y;j<=max_y;j++){
        int16_t i;for( i=min_x/8;i<=max_x/8;i++){
				tem=bitmap[i+j*col_max];//j*so cot
		if (tem){ // check 0x00;
            uint8_t k;
        for(k=0;k<8;k++){
		if((tem&0x80)&&(i*8+k)>=min_x&&(i*8+k)<max_x){ 
			_x=i*8+k+tran_x;
			_y=j+tran_y;

		if(_x>=limit_x1&&_x<limit_x2) {pix_x=_x;STATE_X=true;}else if(roll) {STATE_X=true;if(_x-limit_x1<0&&(limit_x1-_x<(limit_x2-limit_x1))) pix_x=_x+limit_x2-limit_x1;
		else if (_x-limit_x2>0&&(_x-limit_x2<(limit_x2-limit_x1))) pix_x=_x-limit_x2+limit_x1; else STATE_X=false;} else STATE_X=false;
		if(_y>=limit_y1&&_y<limit_y2) {pix_y=_y;STATE_Y=true;}else if(roll) {STATE_Y=true;if(_y-limit_y1<0&&(limit_y1-_y<(limit_y2-limit_y1))) pix_y=_y+limit_y2-limit_y1;
		else if (_y-limit_y2>0&&(_y-limit_y2<(limit_y2-limit_y1))) pix_y=_y-limit_y2+limit_y1; else STATE_Y=false;} else STATE_Y=false;
		if((STATE_X)&&(STATE_Y)) drawPixel(pix_x,pix_y,SSD1306.textcolor);							
		}
				tem<<=1;
		}	}		
		}	
		}	}
	//*************************************************************************************
//========================== SCREEN LARGER =============================//

void drawbitmap_Interleaved_roll(const uint8_t *bitmap,uint8_t col_max,int16_t limit_x1, int16_t limit_x2,int16_t  limit_y1,
	int16_t  limit_y2, uint16_t min_x,uint16_t max_x,uint16_t min_y ,uint16_t max_y,int16_t tran_x,int16_t tran_y,bool roll)
	{
		uint8_t tem=0;
		int16_t _x,_y,pix_x=0,pix_y=0;
		bool STATE_X=false,STATE_Y=false;
        int16_t j;for(j=min_y;j<=max_y;j++){
        int16_t i;for( i=min_x/8;i<=max_x/8;i++){
   // if((pix_x%2==0&&pix_y%2==0)||(pix_x%2!=0&&pix_y%2!=0))STATE_INTER=true; else 	STATE_INTER=false;		
				tem=bitmap[i+j*col_max];//j*so cot
		if (tem){ // check 0x00;
        uint8_t k ;for( k=0;k<8;k++){
		if((tem&0x80)&&(i*8+k)>=min_x&&(i*8+k)<max_x){ 
			_x=i*8+k+tran_x;
			_y=j+tran_y;

		if(_x>=limit_x1&&_x<limit_x2) {pix_x=_x;STATE_X=true;}else if(roll) {STATE_X=true;if(_x-limit_x1<0&&(limit_x1-_x<(limit_x2-limit_x1))) pix_x=_x+limit_x2-limit_x1;
		else if (_x-limit_x2>0&&(_x-limit_x2<(limit_x2-limit_x1))) pix_x=_x-limit_x2+limit_x1; else STATE_X=false;} else STATE_X=false;
		if(_y>=limit_y1&&_y<limit_y2) {pix_y=_y;STATE_Y=true;}else if(roll) {STATE_Y=true;if(_y-limit_y1<0&&(limit_y1-_y<(limit_y2-limit_y1))) pix_y=_y+limit_y2-limit_y1;
		else if (_y-limit_y2>0&&(_y-limit_y2<(limit_y2-limit_y1))) pix_y=_y-limit_y2+limit_y1; else STATE_Y=false;} else STATE_Y=false;
		if((STATE_X)&&(STATE_Y)&&((pix_x%2==0&&pix_y%2==0)||(pix_x%2!=0&&pix_y%2!=0))) drawPixel(pix_x,pix_y,SSD1306.textcolor);							
		}
				tem<<=1;
		}	}		
		}	
		}	}
	//*************************************************************************************		
		//
		void fillrect_roll(int16_t limit_x1, int16_t limit_x2,int16_t  limit_y1,
	int16_t  limit_y2, uint16_t min_x,uint16_t max_x,uint16_t min_y ,uint16_t max_y,int16_t tran_x,int16_t tran_y,bool roll)
	{
		//uint8_t tem=0;
		int16_t _x,_y,pix_x=0,pix_y=0;
		bool STATE_X=false,STATE_Y=false;
        uint8_t j ;for( j=min_y;j<=max_y;j++){
        uint8_t  i;for( i=min_x/8;i<=max_x/8;i++){

        uint8_t  k;for( k=0;k<8;k++){
		if((i*8+k)>=min_x&&(i*8+k)<max_x){ 
			_x=i*8+k+tran_x;
			_y=j+tran_y;

		if(_x>=limit_x1&&_x<limit_x2) {pix_x=_x;STATE_X=true;}else if(roll) {STATE_X=true;if(_x-limit_x1<0&&(limit_x1-_x<(limit_x2-limit_x1))) pix_x=_x+limit_x2-limit_x1;
		else if (_x-limit_x2>0&&(_x-limit_x2<(limit_x2-limit_x1))) pix_x=_x-limit_x2+limit_x1; else STATE_X=false;} else STATE_X=false;
		if(_y>=limit_y1&&_y<limit_y2) {pix_y=_y;STATE_Y=true;}else if(roll) {STATE_Y=true;if(_y-limit_y1<0&&(limit_y1-_y<(limit_y2-limit_y1))) pix_y=_y+limit_y2-limit_y1;
		else if (_y-limit_y2>0&&(_y-limit_y2<(limit_y2-limit_y1))) pix_y=_y-limit_y2+limit_y1; else STATE_Y=false;} else STATE_Y=false;
		if((STATE_X)&&(STATE_Y)) drawPixel(pix_x,pix_y,SSD1306.textcolor);							
		}

		}			
		}	
		}	}
	//
		bool check_section(int16_t x,int16_t x1,int16_t x2,int16_t l1,int16_t l2){
	bool status=false;
	int16_t tem=l2-l1;
	if(x1>=l1&&x2<=l2){ if(x>=x1&&x<=x2)status =true;}
	else if(x2>l2&&x1>l2){if((x>x1-tem)&&(x<x2-tem)) status=true ;}
	else if(x1<l1&&x2<l1){if((x>x1+tem)&&(x<x2+tem)) status=true ;}
	else if(x1<=l2&&x2>=l2) {if((x>l1&&x<x2-tem)||(x>=x1&&x<=l2)) status=true;}
  else if(x1<=l1&&x2>=l1) {if((x>=l1&&x<=x2)||(x>x1+tem&&x<l2)) status=true;}
	return status ;	
}		
		//************************************************************************************
		void drawbitmap_roll_invert(const uint8_t *bitmap,uint8_t col_max,int16_t limit_x1, int16_t limit_x2,int16_t  limit_y1,
	int16_t  limit_y2, uint16_t min_x,uint16_t max_x,uint16_t min_y ,uint16_t max_y,int16_t tran_x,int16_t tran_y,bool roll,
	int16_t inv_x1,int16_t inv_x2,int16_t inv_y1,int16_t inv_y2)	
	{
		uint8_t tem=0;
		int16_t _x,_y,pix_x=0,pix_y=0;
		bool STATE_X=false,STATE_Y=false;
        uint8_t j ;for( j=min_y;j<=max_y;j++){
        uint8_t  i;for( i=min_x/8;i<=max_x/8;i++){
				tem=bitmap[i+j*col_max];//j*so cot
		if (tem){ // check 0x00;
        uint8_t  k;for( k=0;k<8;k++){
			_x=i*8+k+tran_x;
			_y=j+tran_y;
		if((tem&0x80)&&(i*8+k)>=min_x&&(i*8+k)<max_x){ 

		if(_x>=limit_x1&&_x<limit_x2) {pix_x=_x;STATE_X=true;}else if(roll) {STATE_X=true;if(_x-limit_x1<0&&(limit_x1-_x<(limit_x2-limit_x1))) pix_x=_x+limit_x2-limit_x1;
		else if (_x-limit_x2>0&&(_x-limit_x2<(limit_x2-limit_x1))) pix_x=_x-limit_x2+limit_x1; else STATE_X=false;} else STATE_X=false;
		if(_y>=limit_y1&&_y<limit_y2) {pix_y=_y;STATE_Y=true;}else if(roll) {STATE_Y=true;if(_y-limit_y1<0&&(limit_y1-_y<(limit_y2-limit_y1))) pix_y=_y+limit_y2-limit_y1;
		else if (_y-limit_y2>0&&(_y-limit_y2<(limit_y2-limit_y1))) pix_y=_y-limit_y2+limit_y1; else STATE_Y=false;} else STATE_Y=false;
		if((STATE_X)&&(STATE_Y)){if(check_section(pix_x,inv_x1,inv_x2,0,128)&&check_section(pix_y,inv_y1,inv_y2,10,64)){}else drawPixel(pix_x,pix_y,SSD1306.textcolor);
		}							
		} 
				tem<<=1;
		}	}	
		////x:0->128,y: 10->64:la vung can invert.
		tem=bitmap[i+j*col_max];//j*so cot
		if (tem!=0xff){ // check 0xff;
        uint8_t  k;for( k=0;k<8;k++){
			_x=i*8+k+tran_x;
			_y=j+tran_y;
		if(((tem|0x7f)==0x7f)&&(i*8+k)>=min_x&&(i*8+k)<max_x){ 

		if(_x>=limit_x1&&_x<limit_x2) {pix_x=_x;STATE_X=true;}else if(roll) {STATE_X=true;if(_x-limit_x1<0&&(limit_x1-_x<(limit_x2-limit_x1))) pix_x=_x+limit_x2-limit_x1;
		else if (_x-limit_x2>0&&(_x-limit_x2<(limit_x2-limit_x1))) pix_x=_x-limit_x2+limit_x1; else STATE_X=false;} else STATE_X=false;
		if(_y>=limit_y1&&_y<limit_y2) {pix_y=_y;STATE_Y=true;}else if(roll) {STATE_Y=true;if(_y-limit_y1<0&&(limit_y1-_y<(limit_y2-limit_y1))) pix_y=_y+limit_y2-limit_y1;
		else if (_y-limit_y2>0&&(_y-limit_y2<(limit_y2-limit_y1))) pix_y=_y-limit_y2+limit_y1; else STATE_Y=false;} else STATE_Y=false;
		if((STATE_X)&&(STATE_Y)){if(check_section(pix_x,inv_x1,inv_x2,0,128)&&check_section(pix_y,inv_y1,inv_y2,10,64))drawPixel(pix_x,pix_y,SSD1306.textcolor);
		}							
		} 
				tem<<=1;
		}	}	
		//
		}	
		}	}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//***********************************************************************

//
void drawbitmap_roll_timer(const uint8_t *bitmap,uint8_t col_max,int16_t limit_x1, int16_t limit_x2,int16_t  limit_y1,
	int16_t  limit_y2, uint16_t min_x,uint16_t max_x,uint16_t min_y ,uint16_t max_y,int16_t tran_x,int16_t tran_y,bool roll)
	{
		uint8_t tem=0;
		int16_t _x,_y,pix_x=0,pix_y=0;
		bool STATE_X=false,STATE_Y=false;
        uint8_t j ;for( j=min_y;j<max_y;j++){
        uint8_t  i;for( i=min_x/8;i<max_x/8;i++){
				tem=bitmap[i+j*col_max];//j*so cot
		if (tem){ // check 0x00;
        uint8_t  k;for( k=0;k<8;k++){
		if((tem&0x80)&&(i*8+k)>=min_x&&(i*8+k)<max_x){ 
			_x=i*8+k+tran_x;
			_y=j+tran_y;

		if(_x>=limit_x1&&_x<limit_x2) {pix_x=_x;STATE_X=true;}else if(roll) {STATE_X=true;if(_x-limit_x1<0&&(limit_x1-_x<(limit_x2-limit_x1))) pix_x=_x+limit_x2-limit_x1;
		else if (_x-limit_x2>0&&(_x-limit_x2<(limit_x2-limit_x1))) pix_x=_x-limit_x2+limit_x1; else STATE_X=false;} else STATE_X=false;
		if(_y>=limit_y1&&_y<limit_y2) {pix_y=_y;STATE_Y=true;}else if(roll) {STATE_Y=true;if(_y-limit_y1<0&&(limit_y1-_y<(limit_y2-limit_y1))) pix_y=_y+limit_y2-limit_y1;
		else if (_y-limit_y2>0&&(_y-limit_y2<(limit_y2-limit_y1))) pix_y=_y-limit_y2+limit_y1; else STATE_Y=false;} else STATE_Y=false;
		if((STATE_X)&&(STATE_Y)) drawPixel(pix_x,pix_y,SSD1306.textcolor);							
		}
				tem<<=1;
		}	
		}		
		}	
		}	
	}		
//*************************************************************************//
void draw_char_roll(int32_t x, int32_t y, unsigned char c, uint16_t color, uint16_t bg, uint8_t size,int16_t limit_x1,int16_t limit_x2,
	int16_t limit_y1,int16_t limit_y2,int16_t tran_x,int16_t tran_y,bool roll)
{
	if ((x >= SSD1306._width)		||	// Clip right
	   (y >= SSD1306._height)		||	// Clip bottom
	   ((x + 6 * size - 1) < 0)		||	// Clip left
	   ((y + 8 * size - 1) < 0))		// Clip top
		return;
bool STATE_X=false,STATE_Y=false;	
int16_t _x,_y,pix_x,pix_y;
    int8_t i; for ( i=0; i<6; i++ )
	{
		uint8_t line;
		if (i == 5)
			line = 0x0;//space
		else
			line = font[(c*5)+i];
        int8_t j; for ( j = 0; j<8; j++)
		{
			_x=x+i*size+tran_x;
			_y=y+j*size+tran_y;
			if (line & 0x1) //1
			{
					if(_x>=limit_x1&&_x<limit_x2) {pix_x=_x;STATE_X=true;}else if(roll) {STATE_X=true;if(_x-limit_x1<0&&(limit_x1-_x<(limit_x2-limit_x1))) pix_x=_x+limit_x2-limit_x1;
					else if (_x-limit_x2>0&&(_x-limit_x2<(limit_x2-limit_x1))) pix_x=_x-limit_x2+limit_x1; else STATE_X=false;} else STATE_X=false;
					if(_y>=limit_y1&&_y<limit_y2) {pix_y=_y;STATE_Y=true;}else if(roll) {STATE_Y=true;if(_y-limit_y1<0&&(limit_y1-_y<(limit_y2-limit_y1))) pix_y=_y+limit_y2-limit_y1;
					else if (_y-limit_y2>0&&(_y-limit_y2<(limit_y2-limit_y1))) pix_y=_y-limit_y2+limit_y1; else STATE_Y=false;} else STATE_Y=false;
				  if((STATE_X)&&(STATE_Y)){
						if (size == 1) // default size
					drawPixel(pix_x, pix_y, color);
				else {  // big size
					fillRect(pix_x,pix_y , size, size, color);
				}}

			}
			else if (bg != color)
			{
					if(_x>=limit_x1&&_x<limit_x2) {pix_x=_x;STATE_X=true;}else if(roll) {STATE_X=true;if(_x-limit_x1<0&&(limit_x1-_x<(limit_x2-limit_x1))) pix_x=_x+limit_x2-limit_x1;
					else if (_x-limit_x2>0&&(_x-limit_x2<(limit_x2-limit_x1))) pix_x=_x-limit_x2+limit_x1; else STATE_X=false;} else STATE_X=false;
					if(_y>=limit_y1&&_y<limit_y2) {pix_y=_y;STATE_Y=true;}else if(roll) {STATE_Y=true;if(_y-limit_y1<0&&(limit_y1-_y<(limit_y2-limit_y1))) pix_y=_y+limit_y2-limit_y1;
					else if (_y-limit_y2>0&&(_y-limit_y2<(limit_y2-limit_y1))) pix_y=_y-limit_y2+limit_y1; else STATE_Y=false;} else STATE_Y=false;
				if((STATE_X)&&(STATE_Y)){
				if (size == 1) // default size
					drawPixel(pix_x, pix_y, bg);
				else {  // big size
					fillRect(pix_x, pix_y, size, size, bg);
				}}

			}
			line >>= 1;
		}
	}
}
//*********************************************************************************************
void write_char_roll(uint8_t c,int16_t limit_x1,int16_t limit_x2,int16_t limit_y1,int16_t limit_y2,int16_t tran_x,int16_t tran_y,bool roll)
{
	if (c == '\n')
	{
		SSD1306.cursor_y += SSD1306.textsize*8;
		SSD1306.cursor_x  = 0;
	}
	else if (c == '\r') {
	// skip me
	}
	else
	{
		draw_char_roll(SSD1306.cursor_x, SSD1306.cursor_y, c, SSD1306.textcolor, SSD1306.textbgcolor, SSD1306.textsize,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
		SSD1306.cursor_x += SSD1306.textsize*6;
		if (SSD1306.wrap && (SSD1306.cursor_x > (SSD1306._width - SSD1306.textsize*6)))
		{
			SSD1306.cursor_y += SSD1306.textsize*8;//when over limit ,then auto down a row
			SSD1306.cursor_x = 0;
		}
	}
}
//======================================khong tu dong xuong hang================================================//
void write_char_roll_ex(uint8_t c,int16_t limit_x1,int16_t limit_x2,int16_t limit_y1,int16_t limit_y2,int16_t tran_x,int16_t tran_y,bool roll)
{
 
		if (c == '\r') {
	// skip me
			}
	else
	{
		draw_char_roll(SSD1306.cursor_x, SSD1306.cursor_y, c, SSD1306.textcolor, SSD1306.textbgcolor, SSD1306.textsize,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
	}
}
//**************************************cuon string********************************************************
void print_roll(const char *String,int16_t limit_x1,int16_t limit_x2,int16_t limit_y1,int16_t limit_y2,int16_t tran_x,int16_t tran_y,bool roll)
{
	while (*String != 0x00)
	{
		write_char_roll(*String, limit_x1,limit_x2, limit_y1, limit_y2, tran_x, tran_y, roll);
		String++;
	}
}

/************************************** END FILE ****************************************/
void print_roll_ver(const char *String,int16_t limit_x1,int16_t limit_x2,int16_t limit_y1,int16_t limit_y2,int16_t tran_x,int16_t tran_y,bool roll)
{

	while (*String != 0x00)
	{	
	write_char_roll_ex(*String, limit_x1,limit_x2, limit_y1, limit_y2, tran_x, tran_y, roll);		
	tran_y=tran_y+8*SSD1306.textsize;	
		String++;
	}
}
//*****************************************************************************************/
void print_roll_hol(const char *String,int16_t limit_x1,int16_t limit_x2,int16_t limit_y1,int16_t limit_y2,int16_t tran_x,int16_t tran_y,bool roll)
{

	while (*String != 0x00)
	{
		
	write_char_roll_ex(*String, limit_x1,limit_x2, limit_y1, limit_y2, tran_x, tran_y, roll);		
	tran_x=tran_x+6*SSD1306.textsize;	
		String++;
	}
}

/**************************************************************************************************/
//
void printDigit_roll(uint8_t Digit,int16_t limit_x1,int16_t limit_x2,int16_t limit_y1,int16_t limit_y2,int16_t tran_x,int16_t tran_y,bool roll)
{
	if (Digit < 10)
	{
		uint8_t character = 48 + Digit;
		write_char_roll(character,limit_x1,limit_x2, limit_y1, limit_y2, tran_x, tran_y, roll) ;
	}
	else if (Digit < 16)
	{
		uint8_t character = 55 + Digit;
		write(character);
	}
}


//**************************************************************************************
void printNumber_roll(int32_t Number, bool Spaces,int16_t limit_x1,int16_t limit_x2,int16_t limit_y1,int16_t limit_y2,int16_t tran_x,int16_t tran_y,bool roll)
{
	uint8_t ones, tens, hundreds, thousands, tenThousands, hundredThousands, millions, tenMillions, hundredMillions, billions;
  if (Number < 0) print("-");
	Number =abs(Number);
	billions = Number / 1000000000;
	Number = Number % 1000000000;

	hundredMillions = Number / 100000000;
	Number = Number % 100000000;

	tenMillions = Number / 10000000;
	Number = Number % 10000000;

	millions = Number / 1000000;
	Number = Number % 1000000;

	hundredThousands = Number / 100000;
	Number = Number % 100000;

	tenThousands = Number / 10000;
	Number = Number % 10000;

	thousands = Number / 1000;
	Number = Number % 1000;

	hundreds = Number / 100;
	Number = Number % 100;

	tens = Number / 10;
	Number = Number % 10;

	ones = Number;

	if (billions)
	{
		printDigit_roll((uint8_t)billions,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
		if (Spaces) print(" ");
	}
	if (hundredMillions || billions)
	{
		printDigit_roll((uint8_t)hundredMillions,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
	}
	if (tenMillions || hundredMillions || billions)
	{
		printDigit_roll((uint8_t)tenMillions,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
	}
	if (millions || tenMillions || hundredMillions || billions)
	{
		printDigit_roll((uint8_t)millions,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
		if (Spaces) print(" ");
	}
	if (hundredThousands || millions || tenMillions || hundredMillions || billions)
	{
		printDigit_roll((uint8_t)hundredThousands,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
	}
	if (tenThousands || hundredThousands || millions || tenMillions || hundredMillions || billions)
	{
		printDigit_roll((uint8_t)tenThousands,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
	}
	if (thousands || tenThousands || hundredThousands || millions || tenMillions || hundredMillions || billions)
	{
		printDigit_roll((uint8_t)thousands,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
		if (Spaces) print(" ");
	}
	if (hundreds || thousands || tenThousands || hundredThousands || millions || tenMillions || hundredMillions || billions)
	{
		printDigit_roll((uint8_t)hundreds,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
	}
	if (tens || hundreds || thousands || tenThousands || hundredThousands || millions || tenMillions || hundredMillions || billions)
	{
		printDigit_roll((uint8_t)tens,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
	}
	printDigit_roll((uint8_t)ones,limit_x1,limit_x2,limit_y1,limit_y2,tran_x,tran_y,roll);
}

//*************************************************
//===============================  END SCREEN LARGER  ==================================// 
/*************************************Binh Trong ***************************************/
/********************Add some functions 02/08/2018**************************************/
void printFromPoint(const char *String,uint16_t point)
{
	String = String + point;
	while (*String != 0x00)
	{
		write(*String);
		String++;
	}
}
void drawBitmapCircle(int32_t x, int32_t y, uint8_t *bitmap, int32_t w, int32_t h, int32_t r, uint16_t color)
{
	  int32_t i, j, byteWidth = (w + 7) / 8;
    uint8_t byte = 0;

    for(j=0; j<h; j++) {
        for(i=0; i<w; i++ ) {
            if(i & 7) byte <<= 1;
            else      byte   = bitmap[j * byteWidth + i / 8];
            if(byte & 0x80) 
						{
							float tmp = sqrt(pow(w/2-i,2)+pow(h/2-j,2));
							if(tmp<=r)
							drawPixel(x+i, y+j, color);
						}
        }
    }
}

void drawBitmapLimit_X(int32_t x, int32_t y, uint8_t *bitmap, int32_t w, int32_t h, int32_t limitDown, int32_t limitUp, uint16_t color)
{
	  int32_t i, j, byteWidth = (w + 7) / 8;
    uint8_t byte = 0;

    for(j=0; j<h; j++) {
        for(i=0; i<w; i++ ) {
            if(i & 7) byte <<= 1;
            else      byte   = bitmap[j * byteWidth + i / 8];
            if(byte & 0x80) 
						{
							if((x+i)>= limitDown && (x+i)< limitUp)
							drawPixel(x+i, y+j, color);
						}
        }
    }
}
void drawBitmapLimit_Y(int32_t x, int32_t y, uint8_t *bitmap, int32_t w, int32_t h, int32_t limitDown, int32_t limitUp, uint16_t color)
{
	  int32_t i, j, byteWidth = (w + 7) / 8;
    uint8_t byte = 0;

    for(j=0; j<h; j++) {
        for(i=0; i<w; i++ ) {
            if(i & 7) byte <<= 1;
            else      byte   = bitmap[j * byteWidth + i / 8];
            if(byte & 0x80) 
						{
							if((y+j)>= limitDown && (y+j)< limitUp)
							drawPixel(x+i, y+j, color);
						}
        }
    }
}

void drawARC(int32_t x, int32_t y, int32_t r,uint16_t color, int32_t startAngle, int32_t endAngle,int32_t disAngle,int32_t rc,uint8_t typeDraw)
{
	if(typeDraw==0){
        int32_t i;for( i = startAngle; i<= endAngle;i=i+disAngle)
		{
			int32_t px = x + (int32_t)round(r*sin(RADS*i));
			int32_t py = y + (int32_t)round(r*cos(RADS*i));
			drawPixel(px,py,color);
		}
	}else if(typeDraw==1){
		
        int32_t i;for( i = startAngle; i<= endAngle;i=i+disAngle){
			int32_t px = x + (int32_t)round(r*sin(RADS*i));
			int32_t py = y + (int32_t)round(r*cos(RADS*i));
			if(py>=0 && py<=64)
			fillCircle(px,py,rc,color);
		}
	}
}
void fillRect_limit(int32_t x, int32_t y, int32_t w, int32_t h, uint16_t color,int16_t min_y,int16_t max_y)
{
    int32_t i;  for ( i=x; i<x+w; i++)
	{
		if(y>min_y && y<(max_y-h))
		drawFastVLine(i, y, h, color);
		else if(y<min_y && (min_y-y)<h && (min_y-y)>0)drawFastVLine(i, min_y, h-(min_y-y), color);
		else if(y>(max_y-h) && (max_y-y)<h && (max_y-y)>0)drawFastVLine(i, (max_y-h), h-(max_y-y), color);
	}
}
//THUAN===========================================================================================================================================
//direct_limit=1: >xlimit,>ylimit
//direct_limit=0: <xlimit,<ylimit
void drawChar_limit(int32_t x, int32_t y, int x_limit, int y_limit,unsigned char c, uint16_t color, uint16_t bg, uint8_t direct_limit, uint8_t size)
{
	if ((x >= SSD1306._width)		||	// Clip right
	   (y >= SSD1306._height)		||	// Clip bottom
	   ((x + 6 * size - 1) < 0)		||	// Clip left
	   ((y + 8 * size - 1) < 0))		// Clip top
		return;

    int8_t i; for ( i=0; i<6; i++ )
	{
		uint8_t line;
		if (i == 5)
			line = 0x0;//space
		else
			// TODO: was progmem here before so test if this actually works!
			line = font[(c*5)+i];
        int8_t j; for ( j = 0; j<8; j++)
		{
			if (line & 0x1) //1
			{
				if(direct_limit==1)
				{
					if((x+(i*size))>x_limit&&(y+(j*size))>y_limit)
					{
						if (size == 1) // default size
							drawPixel(x+i, y+j, color);
						else {  // big size
							fillRect(x+(i*size), y+(j*size), size, size, color);
						}
					}
					
				}
				
				if(direct_limit==0)
				{
					if((x+(i*size))<x_limit&&(y+(j*size)<y_limit))
					{
						if (size == 1) // default size
							drawPixel(x+i, y+j, color);
						else {  // big size
							fillRect(x+(i*size), y+(j*size), size, size, color);
						}
					}
					
				}
				
			}
			else if (bg != color)
			{
				if(direct_limit==1)
				{
					if((x+(i*size))>x_limit&&(y+(j*size))>y_limit)
					{
						if (size == 1) // default size
							drawPixel(x+i, y+j, bg);
						else {  // big size
							fillRect(x+i*size, y+j*size, size, size, bg);
						}
					}
					
				}
				if(direct_limit==0)
				{
					if((x+(i*size))<x_limit&&(y+(j*size))<y_limit)
					{
						if (size == 1) // default size
							drawPixel(x+i, y+j, bg);
						else {  // big size
							fillRect(x+i*size, y+j*size, size, size, bg);
						}
					}
					
				}
				
			}
			line >>= 1;
		}
	}
}
void write_limit(uint8_t c, int x_limit, int y_limit, uint8_t direct_limit)
{
	if (c == '\n')
	{
		SSD1306.cursor_y += SSD1306.textsize*8;
		SSD1306.cursor_x  = 0;
	}
	else if (c == '\r') {
	// skip me
	}
	else
	{
		drawChar_limit(SSD1306.cursor_x, SSD1306.cursor_y, x_limit, y_limit, c, SSD1306.textcolor, SSD1306.textbgcolor, direct_limit, SSD1306.textsize);
		SSD1306.cursor_x += SSD1306.textsize*6;
		if (SSD1306.wrap && (SSD1306.cursor_x > (SSD1306._width - SSD1306.textsize*6)))
		{
			SSD1306.cursor_y += SSD1306.textsize*8;
			SSD1306.cursor_x = 0;
		}
	}
}
void print_limit(const char *String, int x_limit, int y_limit, uint8_t direct_limit)
{
	while (*String != 0x00)
	{
		write_limit(*String,x_limit, y_limit, direct_limit);
		String++;
	}
}
/*************************************The end*******************************************/

