#include "uboot_app.h"

#include "gpio_base.h"
#include "ssd1306.h"
#include "Adafruit_SSD1306.h"
#include "glcdfont.c"
#include "power.h"
#define BUTTON 13
#define EN_PW_PIN 12  //GPIOA13

#define BUZZER    12  //GPIOD12
#define ON 1
#define OFF 0

void button_init()
{
    printf("______________button_init________________ \n");
    GPIO_AINEN   |= (1 << BUTTON);
}
int get_button(int bit)
{
    int state;
    state = (GPIO_ADAT >> BUTTON) & 1U;
    printf("BT= %d\n",state);
    return state;
}
void en_pw()
{
    GPIO_AOUTEN |= (1 << EN_PW_PIN);
    GPIO_ADAT   |= (1 << EN_PW_PIN);
}

int start()
{
    button_init();
    begin(SSD1306_SWITCHCAPVCC,SSD1306_I2C_ADDRESS);
    mdelay(100);
    setTextColor(WHITE);
    setTextSize(1);
    setCursor(0,0);
    clearDisplay();
    drawBitmap(0,0,(uint8_t*)img_furious_turnoff_64_128,128,64,1);
    display();
    printf("display logo fpv");
    mdelay(100);

    while (get_button(BUTTON)) {
        mdelay(50);
    }


    clearDisplay();
    setCursor(3,10);
    print("press any key to start");
    display();

    while (!get_button(BUTTON)) {
        mdelay(50);
        printf("!get_button\n");
    }

    int i=0;
    while(1)
    {
         printf("while(1)");
        for(i=0;i<128;)
        {
             printf("%d",i);
             if(get_button(BUTTON)==0)
             {
             i=i+2;
             drawRect(0,20,i,10,WHITE);
             }
             display();
        }

        if(i>=128) break;
    }

    en_pw();
    clearDisplay();
    setCursor(15,10);
    print("  START KERNEL");
    display();
}

void welcom()
{

    start();
}
