#include "gpio_base.h"



void gpio_set(int port,int pin)
{
    port |= 1 << pin;
}
void gpio_clear(int port,int pin)
{
    port &= ~(1 << pin);
}
int gpio_get(int port,int pin)
{
    pin = 0x0001 << pin;
    return(pin & port);
}

void setmode(int reg, int pin,int mode )
{
    if(mode==SET)
    {
        gpio_set(reg,pin);
    }
    else
    {
        gpio_clear(reg,pin);
    }
}
