
#include "i2c_sofware.h"
#include <stddef.h>
#define DEFAULT_ndelay	5
#define RETRIES		0
#define I2C_ACK		0
#define I2C_NOACK	1

//define OLED_I2C_DBG
#ifdef OLED_I2C_DBG
#define oled_dbg(fmt, args...)	printf(fmt, ##args)
#define i2c_dbg(fmt, args...)	printf(fmt, ##args)
#else
#define oled_dbg(fmt, args...)
#define i2c_dbg(fmt, args...)
#endif

int speed=1;

#define SDA 1  //E1
#define SCL 0  //E0


void *_memcpy (void *dest, const void *src, size_t len)
{
  char *d = dest;
  const char *s = src;
  while (len--)
    *d++ = *s++;
  return dest;
}

enum {
    INPUT=0,
    OUTPUT=1
};
enum {
    PIN_SDA = 0,
    PIN_SCL = 1,
    PIN_COUNT
};


void ndelay(int delay)
{
    while ( delay > 0 ){
      delay--;
    }
}


void close_porte()
{
        GPIO_EOUTEN = 0x0000;
        GPIO_EINEN = 0x0000;
}
void setbit(int bit)
{
    GPIO_EDAT |= 1 << bit;
}
void clearbit(int bit)
{
    GPIO_EDAT &= ~(1 << bit);
}
int getbit(int bit)
{
    bit = 0x0001 << bit;
    return(bit & GPIO_EDAT);
}
int setmode_sda(int IN_OUT)
{
    if(IN_OUT==INPUT)
    {
        GPIO_EOUTEN &= ~(1 << SDA);    //disable ouput
        GPIO_EINEN  |= 1 << SDA;        //enable input
    }
    else
    {
        GPIO_EINEN   &= ~(1 << SDA);     //disable input
        GPIO_EOUTEN  |= 1 << SDA;      //enable ouput
    }
}


void led_life_init() {
    printf("led_life_init\n");
    GPIO_EOUTEN =0x0003;
}
void led_life_on(void) {
    printf("led_life_on *\n");
    setbit(0);
}

void led_life_off(void) {
    printf("led_life_off *\n");
    clearbit(0);
}

void oled_i2c_init(){
    oled_dbg("oled_i2c_init\n");
    GPIO_EINEN =0x0003;  //set oput
    GPIO_EOUTEN=0x0003;  //set input
}
static int i2c_gpio_sda_get()
{
    int ret = getbit(SDA);
    return ret;
}
static void i2c_gpio_sda_set( int bit)
{
    setmode_sda(OUTPUT);
    if (bit)
    {
        setbit(SDA);
    }
    else
    {
        clearbit(SDA);
        //      setmode_sda(INPUT);
    }
}
static void i2c_gpio_scl_set(int bit)
{

    if (bit)
        setbit(SCL);
    else
        clearbit(SCL);

}

static void i2c_gpio_write_bit(int delay, uchar bit)
{
    i2c_gpio_scl_set(0);
    ndelay(delay);
    i2c_gpio_sda_set(bit);
    ndelay(delay);
    i2c_gpio_scl_set(1);
    ndelay(2 * delay);
}

static int i2c_gpio_read_bit(int delay)
{
    int value;
    i2c_gpio_scl_set(1);
    ndelay(delay);
    value = i2c_gpio_sda_get();
    ndelay(delay);
    i2c_gpio_scl_set( 0);
    ndelay(2 * delay);
    return value;
}

/* START: High -> Low on SDA while SCL is High */
static void i2c_gpio_send_start(int delay)
{
    ndelay(delay);
    i2c_gpio_sda_set(1);
    ndelay(delay);
    i2c_gpio_scl_set(1);
    ndelay(delay);
    i2c_gpio_sda_set(0);
    ndelay(delay);
}
/* STOP: Low -> High on SDA while SCL is High */
static void i2c_gpio_send_stop(int delay)
{
    i2c_gpio_scl_set(0);
    ndelay(delay);
    i2c_gpio_sda_set(0);
    ndelay(delay);
    i2c_gpio_scl_set(1);
    ndelay(delay);
    i2c_gpio_sda_set(1);
    ndelay(delay);
}
/* ack should be I2C_ACK or I2C_NOACK */
static void i2c_gpio_send_ack(int delay, int ack)
{
    i2c_gpio_write_bit(delay, ack);
    i2c_gpio_scl_set( 0);
    ndelay(delay);
}
static void i2c_gpio_send_reset(int delay)
{
    int j;

    for (j = 0; j < 9; j++)
        i2c_gpio_write_bit(delay, 1);

    i2c_gpio_send_stop( delay);
}

/* Set sda high with low clock, before reading slave data */
static void i2c_gpio_sda_high(int delay)
{
    i2c_gpio_scl_set(0);
    ndelay(delay);
    i2c_gpio_sda_set(0);
    setmode_sda(INPUT);
    ndelay(delay);
}
static int i2c_gpio_write_byte(int delay, uchar data)
{
    int j;
    int nack;

    for (j = 0; j < 8; j++) {
        i2c_gpio_write_bit(delay, data & 0x80);
        data <<= 1;
    }

    ndelay(delay);

    /* Look for an <ACK>(negative logic) and return it */
    i2c_gpio_sda_high(delay);
    nack = i2c_gpio_read_bit(delay);
    i2c_dbg("%s: nack= %d \n",__func__, nack);
    return nack;	/* not a nack is an ack */
}
static uchar i2c_gpio_read_byte(int delay, int ack)
{
    int  data;
    int  j;

    i2c_gpio_sda_high(delay);
    data = 0;
    for (j = 0; j < 8; j++) {
        data <<= 1;
        data |= i2c_gpio_read_bit(delay);
    }
    i2c_gpio_send_ack(delay, ack);

    return data;
}

/* send start and the slave chip address */
int i2c_send_slave_addr(int delay,uchar chip)
{
    i2c_gpio_send_start(delay);

    if (i2c_gpio_write_byte(delay, chip)) {
        i2c_gpio_send_stop(delay);
        oled_dbg("I/O error\n");
        return -EIO;
    }

    return 0;
}
extern int i2c_gpio_write_data( uchar chip,
                                uchar* buffer, int len,
                                bool end_with_repeated_start)
{
    unsigned int delay = speed;
    int failures = 0;
    oled_dbg("%s: chip %x buffer %x len %d\n", __func__, chip, buffer[0], len);

    if (i2c_send_slave_addr(delay, chip << 1)) {
        oled_dbg("i2c_write, no chip responded %02X\n", chip);
        return -EIO;
    }

    while (len-- > 0) {
        if (i2c_gpio_write_byte(delay, *buffer++))
            failures++;
    }

    if (!end_with_repeated_start) {
        i2c_gpio_send_stop(delay);
        oled_dbg("%s:i2c_gpio_send_stop\n", __func__);
        return failures;
    }

    if (i2c_send_slave_addr(delay, (chip << 1) | 0x1)) {
        oled_dbg("i2c_write, no chip responded %02X\n", chip);
        return -EIO;
    }

    return failures;
}
static int i2c_gpio_set_bus_speed(unsigned int speed_hz)
{

    speed = 1000000 / (speed_hz << 2);

    i2c_gpio_send_reset(speed);

    return 0;
}

